//
//  Untitled_AppDelegate.h
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QTKit/QTKit.h>


@interface Step8_reduced : NSWindow 
{
	NSPoint initialLocation;
	IBOutlet QTCaptureView *mCaptureView;
	QTCaptureSession           *mCaptureSession;
	QTCaptureMovieFileOutput   *mCaptureMovieFileOutput;
	QTCaptureDeviceInput       *mCaptureDeviceInput;
	IBOutlet NSTextField *countdownField;
	int counter;
	NSButton *playBtn, *stopBtn, *recordBtn, *keepBtn;
	NSInteger pauseTimer;
	
	
}

@property (assign) NSPoint initialLocation;
@property (assign) int counter;
@property (nonatomic, retain) IBOutlet NSButton *playBtn;
@property (nonatomic, retain) IBOutlet NSButton *stopBtn;
@property (nonatomic, retain) IBOutlet NSButton *recordBtn;
@property (nonatomic, retain) IBOutlet NSButton *keepBtn;


-(IBAction)gotoStep4:(id)sender;
- (IBAction)startRecord:(id)sender;
- (IBAction)playVideo:(id)sender;
- (IBAction)stopRecord:(id)sender;
-(IBAction)closeApp:(id)sender;
-(IBAction)expandApp:(id)sender;
-(IBAction)keepVideo:(id)sender;




@end
