//
//  GlobalVars.m
//  VideoCareerFinder
//
//  Created by Ronak on 04/07/11.
//  Copyright 2011 Soft 'n' Web. All rights reserved.
//

#import "GlobalVars.h"

@implementation GlobalVars

NSString *appKey = @"MyApplicationPassword";
NSString *appStatus = @"1";
NSString *appUsername = @"appStatus";
NSString *appFirstLastName = @"John Doe";

static NSString *const defaultsTagName = @"fullname";
static NSString *const defaultsTagFirstName = @"firstname";
static NSString *const defaultsTagLastName = @"lastname";
static NSString *const defaultsTagUsername = @"username";
static NSString *const defaultsTagScript = @"script";
static NSString *const defaultsTagAlignment = @"alignment";
static NSString *const defaultsTagColor = @"color";
static NSString *const defaultsTagFont = @"font";
static NSString *const defaultsTagSpeed = @"speed";

+ (NSUserDefaults *)defaults
{
    return [NSUserDefaults standardUserDefaults];
}

+ (void)setObject:(id)object forKey:(NSString *)key
{
    NSUserDefaults *defaults = [self defaults];

    [defaults setObject:object forKey:key];
    [defaults synchronize];
}

+ (void)removeObjectForKey:(NSString *)key
{
    NSUserDefaults *defaults = [self defaults];

    [defaults removeObjectForKey:key];
    [defaults synchronize];
}

+ (id)valueForKey:(NSString *)key
{
    NSUserDefaults *defaults = [self defaults];

    return [defaults valueForKey:key];
}

+ (void)setFullName:(NSString *)name
{
    [self setObject:name forKey:defaultsTagName];
}

+ (NSString *)getFullName
{
    return [self valueForKey:defaultsTagName];
}

+ (void)setFirstName:(NSString *)name
{
    [self setObject:name forKey:defaultsTagFirstName];
}

+ (NSString *)getFirstName
{
    return [self valueForKey:defaultsTagFirstName];
}

+ (void)setLastName:(NSString *)name
{
    [self setObject:name forKey:defaultsTagLastName];
}

+ (NSString *)getLastName
{
    return [self valueForKey:defaultsTagLastName];
}

+ (void)setUsername:(NSString *)name
{
    [self setObject:name forKey:defaultsTagUsername];
}

+ (NSString *)getUsername
{
    return [self valueForKey:defaultsTagUsername];
}

+ (void)setScript:(NSString *)name
{
    [self setObject:name forKey:defaultsTagScript];
}

+ (NSString *)getScript
{
    return [self valueForKey:defaultsTagScript];
}

+ (void)setAlignment:(NSString *)name
{
    [self setObject:name forKey:defaultsTagAlignment];
}

+ (NSString *)getAlignment
{
    return [self valueForKey:defaultsTagAlignment];
}

+ (void)setColor:(NSString *)name
{
    [self setObject:name forKey:defaultsTagColor];
}

+ (NSString *)getColor
{
    return [self valueForKey:defaultsTagColor];
}

+ (void)setFont:(NSString *)name
{
    [self setObject:name forKey:defaultsTagFont];
}

+ (NSString *)getFont
{
    return [self valueForKey:defaultsTagFont];
}

+ (void)setSpeed:(NSString *)name
{
    [self setObject:name forKey:defaultsTagSpeed];
}

+ (NSString *)getSpeed
{
    return [self valueForKey:defaultsTagSpeed];
}

@end
