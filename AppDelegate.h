//
//  AppDelegate.h
//  VCF_Temp
//
//  Created by Anderson Homer on 7/17/13.
//  Copyright (c) 2013 Anderson Homer. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

@interface AppDelegate : NSObject <NSApplicationDelegate > {
    
    NSTextView *tele_text;
    NSImageView *helptip_img_view;
    NSMutableArray *myarray;
    NSImageView *username_light;
    NSImageView *password_light;
    NSTextField *login_message;
    NSTextField *firstname;
    NSTextField *lastname;
    NSTextField *emailaddress;
    NSTextField *confirmemail;
    NSSecureTextField *password;
    NSSecureTextField *confirmpassword;
    NSTextField *username_field;
    NSSecureTextField *password_field;
    NSTextField *quick_register_message;
}
@property (assign) IBOutlet NSTextView *tele_text;

@property (assign) IBOutlet NSImageView *helptip_img_view;
@property (assign) IBOutlet NSTextField *username_field;
@property (assign) IBOutlet NSSecureTextField *password_field;
@property (assign) IBOutlet NSImageView *username_light;
@property (assign) IBOutlet NSImageView *password_light;

@property (assign) IBOutlet NSTextField *quick_register_message;

@property (assign) IBOutlet NSTextField *login_message;
@property (assign) IBOutlet NSTextField *firstname;
@property (assign) IBOutlet NSTextField *lastname;
@property (assign) IBOutlet NSTextField *emailaddress;
@property (assign) IBOutlet NSTextField *confirmemail;
@property (assign) IBOutlet NSSecureTextField *password;
@property (assign) IBOutlet NSSecureTextField *confirmpassword;
@end
