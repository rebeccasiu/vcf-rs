//
//  Untitled_AppDelegate.h
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

@interface Step1 : NSWindow 
{
	NSPoint initialLocation;
	NSButton *next_btn;
    NSButton *disagree_btn;
    NSButton *agree_btn;
    NSButton *sorryclose_btn;
    NSButton *backtostep1_btn;
    NSImageView *sorry_image_overlay;

    
    IBOutlet WebView *mywebview;
    NSButton *tac_button;
}

@property (assign) NSPoint initialLocation;
@property (nonatomic, retain) IBOutlet NSButton *next_btn;

-(IBAction)gotoStep2:(id)sender;
-(IBAction)closeApp:(id)sender;
- (IBAction)show_tac:(id)sender;
- (IBAction)show_priv:(id)sender;

- (IBAction)goto_step2:(id)sender;

@property (assign) IBOutlet NSButton *tac_button;

@property (assign) IBOutlet NSImageView *sorry_image_overlay;

- (IBAction)backtostep1:(id)sender;
- (IBAction)sorryclose:(id)sender;
- (IBAction)disagree:(id)sender;

@property (assign) IBOutlet NSButton *backtostep1_btn;
@property (assign) IBOutlet NSButton *sorryclose_btn;
@property (assign) IBOutlet NSButton *agree_btn;
@property (assign) IBOutlet NSButton *disagree_btn;

@end
