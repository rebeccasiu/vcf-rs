//
//  Untitled_AppDelegate.h
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import <QTKit/QTKit.h>



@interface Step3_tutorial : NSWindow
{
NSPoint initialLocation;
    NSScrollView *telescroller;
    NSTextView *mytele;
    WebView *mywebview;
    NSString *myglob;
    NSMutableArray *candidatescript_array;
    NSMutableArray *studentscript_array;
    NSMutableArray *recentgradscript_array;
    NSMutableArray *militaryscript_array;
    QTCaptureSession           *mCaptureSession;
	QTCaptureMovieFileOutput   *mCaptureMovieFileOutput;
	QTCaptureDeviceInput       *video, *audio;
    IBOutlet QTCaptureView *mCaptureView;
    IBOutlet QTMovieView *mMovieView;
    NSButton *script_9;
    NSSlider *fontsize_slider_val;
    NSSlider *speed_slider_val;
    NSImageView *helpimage;
    NSButton *stop_test_Scroll_btn;
    NSButton *record_btn;
    NSButton *play_btn;
    NSButton *stop_btn;
    NSButton *upload_btn;
    NSButton *update_Scripts_btn;
    NSImageView *panel2_header;
    NSButton *panel2_grad_btn;
    NSButton *panel2_military_btn;
    NSButton *panel2_student_btn;
    NSButton *panel2_candidate_btn;
    NSImageView *panel2_script_header;
    NSButton *script1_btn;
    NSButton *script2_btn;
    NSButton *script3_btn;
    NSButton *script4_btn;
    NSButton *script5_btn;
    NSButton *script6_btn;
    NSButton *script7_btn;
    NSButton *script8_btn;
    NSButton *script9_btn;
    NSButton *script10_btn;
    NSImageView *panel3_header;
    NSButton *panel3_white;
    NSButton *panel3_Black;
    NSButton *panel3_left_btn;
    NSButton *panel3_center_btn;
    NSImageView *panel3_scroll_header;
    NSButton *panel3_scroll_button;
    NSButton *panel3_manual_Scroll_btn;
    NSButton *panel4_record_btn;
    NSButton *panel2_next2;
    NSButton *panel3_next3;
    NSButton *panel4_next4;
    NSButton *next1_btn;

    int counter,isrecord;
    }


@property (assign) NSPoint initialLocation;
@property (assign) int counter,isrecord;

@property (assign) IBOutlet NSScrollView *telescroller;

@property (assign) IBOutlet NSTextView *mytele;
@property (assign) IBOutlet WebView *mywebview;


- (IBAction)initiate_Recording:(id)sender;

- (IBAction)practice_btn:(id)sender;
- (IBAction)howdone_btn:(id)sender;
- (IBAction)teleprompt_btn:(id)sender;
- (IBAction)mychecklist_btn:(id)sender;
- (IBAction)whatnow_btn:(id)sender;
- (IBAction)candidate_scripts:(id)sender;
- (IBAction)student_scripts:(id)sender;
- (IBAction)recentgrad_scripts:(id)sender;
- (IBAction)military_scripts:(id)sender;

- (IBAction)script_1:(id)sender;
- (IBAction)script_2:(id)sender;

- (IBAction)script_3:(id)sender;
- (IBAction)script_4:(id)sender;
- (IBAction)script_5:(id)sender;
- (IBAction)script_6:(id)sender;
- (IBAction)script_7:(id)sender;
- (IBAction)script_8:(id)sender;
- (IBAction)script_9:(id)sender;
- (IBAction)script_10:(id)sender;

- (IBAction)update_Scripts_btn:(id)sender;
- (IBAction)left_align_btn:(id)sender;
- (IBAction)center_align_btn:(id)sender;
- (IBAction)black_on_white_btn:(id)sender;
- (IBAction)white_on_black_btn:(id)sender;
- (IBAction)fontsize_slider:(id)sender;
@property (assign) IBOutlet NSSlider *fontsize_slider_val;
- (IBAction)test_Scroll_btn:(id)sender;
- (IBAction)speed_slider:(id)sender;
@property (assign) IBOutlet NSSlider *speed_slider_val;
@property (assign) IBOutlet NSImageView *helpimage;
- (IBAction)stop_test_scroll:(id)sender;
@property (assign) IBOutlet NSButton *stop_test_Scroll_btn;

- (IBAction)record_btn_press:(id)sender;
- (IBAction)play_btn_press:(id)sender;
- (IBAction)stop_btn_press:(id)sender;
- (IBAction)upload_btn_press:(id)sender;
- (IBAction)next1:(id)sender;

- (IBAction)close_app:(id)sender;

@property (assign) IBOutlet NSButton *record_btn;
@property (assign) IBOutlet NSButton *play_btn;
@property (assign) IBOutlet NSButton *stop_btn;
@property (assign) IBOutlet NSButton *upload_btn;
@property (assign) IBOutlet NSButton *next1_btn;
@property (assign) IBOutlet NSButton *panel2_candidate_btn;
@property (assign) IBOutlet NSButton *panel2_student_btn;
@property (assign) IBOutlet NSButton *panel2_grad_btn;
@property (assign) IBOutlet NSImageView *panel2_script_header;
@property (assign) IBOutlet NSButton *panel2_military_btn;
@property (assign) IBOutlet NSImageView *panel2_header;
@property (assign) IBOutlet NSButton *script1_btn;
@property (assign) IBOutlet NSButton *script2_btn;
@property (assign) IBOutlet NSButton *script3_btn;
@property (assign) IBOutlet NSButton *script4_btn;
@property (assign) IBOutlet NSButton *script5_btn;
@property (assign) IBOutlet NSButton *script6_btn;
@property (assign) IBOutlet NSButton *script7_btn;
@property (assign) IBOutlet NSButton *script8_btn;
@property (assign) IBOutlet NSButton *script9_btn;
@property (assign) IBOutlet NSButton *script10_btn;
@property (assign) IBOutlet NSButton *update_Scripts_btn;
@property (assign) IBOutlet NSImageView *panel3_header;
@property (assign) IBOutlet NSButton *panel3_white;
@property (assign) IBOutlet NSButton *panel3_Black;

@property (assign) IBOutlet NSButton *panel3_left_btn;
@property (assign) IBOutlet NSButton *panel3_center_btn;
@property (assign) IBOutlet NSImageView *panel3_scroll_header;
@property (assign) IBOutlet NSButton *panel3_scroll_button;
- (IBAction)back_btn:(id)sender;

@property (assign) IBOutlet NSButton *panel3_manual_Scroll_btn;
@property (assign) IBOutlet NSButton *panel4_record_btn;
- (IBAction)next2:(id)sender;
@property (assign) IBOutlet NSButton *panel2_next2;
- (IBAction)next3:(id)sender;
@property (assign) IBOutlet NSButton *panel3_next3;
- (IBAction)next4:(id)sender;
@property (assign) IBOutlet NSButton *panel4_next4;

@end
