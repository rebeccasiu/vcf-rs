//
//  Untitled_AppDelegate.m
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import "ASIFormDataRequest.h"
#import "GlobalVars.h"
#import "Step7_new.h"
#import <AppKit/AppKit.h>
#import "Controller.h"
#import "NSFileManager+DirectoryLocations.h"
#import "Uploader.h"
#import "MyUploader.h";

@implementation Step7_new

@synthesize initialLocation;
@synthesize loadimage;


- (void)advanceTimer:(NSTimer *)timer
{
    counter = counter + 1;
    if (counter > 1) {
		[timer invalidate];

 
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *username = nil;
    
    if (standardUserDefaults)
        username = [standardUserDefaults objectForKey:@"username"];
    

          
        [[MyUploader alloc]initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://www.videocareerfinder.com/app/upload2/%@", username]]
                            filePath: [NSString stringWithFormat:@"%@/tagged.mp4", [[NSFileManager defaultManager] applicationSupportDirectory]]
                            delegate: self
                        doneSelector: @selector(onUploadDone)
                       errorSelector: @selector(onUploadError)];

      
   
   //   NSURL *uploadurl = [NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:8888/php-upload/uploader.php"]];
        
             
        
        
        
    }
}






- (void)loadTimer:(NSTimer *)timer
{
   	imgcounter = imgcounter + 1;
	if(imgcounter==1)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_1.png"]];
	}
	else if(imgcounter==2)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_2.png"]];
	}
	else if(imgcounter==3)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_3.png"]];
	}
	else if(imgcounter==4)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_4.png"]];
	}
	else if(imgcounter==5)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_5.png"]];
	}
	else if(imgcounter==6)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_6.png"]];
	}
	else if(imgcounter==7)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_7.png"]];
	}
	else if(imgcounter==8)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_8.png"]];
		imgcounter = 0;
	}
	
	
	
}




-(void)onUploadDone

{
    [myimagetimer invalidate];
    NSLog(@"Finished Uploading - window class");
	Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step10"];
	
	[myController showWindow:nil];
	[[myController window] makeKeyAndOrderFront:nil];
	[self close];
	[self release];
}

-(void)onUploadError
{
	NSAlert *alert = [[[NSAlert alloc] init] autorelease];
	[alert setMessageText:@"Error"];
	[alert setInformativeText:@"An error has occured during process. Please go back and restart the process."];
	[alert setAlertStyle:NSInformationalAlertStyle];
	[alert beginSheetModalForWindow:self modalDelegate:self didEndSelector:nil contextInfo:nil];
}




- (void)awakeFromNib
{
    
    
	NSTimer *countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1
															   target:self
															 selector:@selector(advanceTimer:)
															 userInfo:nil
															  repeats:YES];
	
 
    
    myimagetimer = [[NSTimer scheduledTimerWithTimeInterval:0.3
                                                     target:self
                                                   selector:@selector(loadTimer:)
                                                   userInfo:nil
                                                    repeats:YES] retain];

    //upload process starts here
    
    
	
}




- (void)dealloc
{
	
	[super dealloc] ;
}



/***********************************/

- (id)initWithContentRect:(NSRect)contentRect
				styleMask:(NSUInteger)windowStyle
				  backing:(NSBackingStoreType)bufferingType
					defer:(BOOL)deferCreation
{
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    
	[self setLevel: NSPopUpMenuWindowLevel];
	
	[self center];
	
	[self setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"step7_background_layout.png"]]];
	
	
	
    return self;	
}

- (void)mouseDown:(NSEvent *)theEvent {    
    // Get the mouse location in window coordinates.
    self.initialLocation = [theEvent locationInWindow];
}


- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect windowFrame = [self frame];
    NSPoint newOrigin = windowFrame.origin;
	
    // Get the mouse location in window coordinates.
    NSPoint currentLocation = [theEvent locationInWindow];
    // Update the origin with the difference between the new mouse location and the old mouse location.
    newOrigin.x += (currentLocation.x - initialLocation.x);
    newOrigin.y += (currentLocation.y - initialLocation.y);
	
    // Don't let window get dragged up under the menu bar
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height)) {
        newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    // Move the window to the new location
    [self setFrameOrigin:newOrigin];
}


@end
