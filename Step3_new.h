//
//  Untitled_AppDelegate.h
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import <QTKit/QTKit.h>



@interface Step3_new : NSWindow
{
NSPoint initialLocation;
    NSScrollView *telescroller;
    NSTextView *mytele;
    QTCaptureSession           *mCaptureSession;
	QTCaptureMovieFileOutput   *mCaptureMovieFileOutput;
	QTCaptureDeviceInput       *video, *audio;
    IBOutlet QTCaptureView *mCaptureView;
    IBOutlet QTMovieView *mMovieView;
    NSSlider *fontsize_slider_val;
    NSSlider *speed_slider_val;
    NSImageView *helpimage;
    NSButton *record_btn;
    NSButton *play_btn;
    NSButton *stop_btn;
    NSButton *upload_btn;
   
    int counter,isrecord;
    }
- (IBAction)close_app:(id)sender;

@property (assign) NSPoint initialLocation;
@property (assign) int counter,isrecord;

@property (assign) IBOutlet NSScrollView *telescroller;

@property (assign) IBOutlet NSTextView *mytele;

- (IBAction)initiate_Recording:(id)sender;



- (IBAction)left_align_btn:(id)sender;
- (IBAction)center_align_btn:(id)sender;
- (IBAction)black_on_white_btn:(id)sender;
- (IBAction)white_on_black_btn:(id)sender;
- (IBAction)fontsize_slider:(id)sender;
@property (assign) IBOutlet NSSlider *fontsize_slider_val;
- (IBAction)speed_slider:(id)sender;
@property (assign) IBOutlet NSSlider *speed_slider_val;
@property (assign) IBOutlet NSImageView *helpimage;



- (IBAction)record_btn_press:(id)sender;
- (IBAction)play_btn_press:(id)sender;
- (IBAction)stop_btn_press:(id)sender;
- (IBAction)upload_btn_press:(id)sender;

@property (assign) IBOutlet NSButton *record_btn;
@property (assign) IBOutlet NSButton *play_btn;
@property (assign) IBOutlet NSButton *stop_btn;
@property (assign) IBOutlet NSButton *upload_btn;
@property (assign) IBOutlet NSTextField *font_label;
@property (assign) IBOutlet NSTextField *speed_label;
@property (assign) IBOutlet NSSegmentedControl *movie_segment;
@property (assign) IBOutlet NSButton *teleprompter_btn;
@property (assign) BOOL *is_teleprompter_scrolling;

@end
