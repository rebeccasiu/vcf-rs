//
//  AppDelegate.m
//  Videocareerfinder
//
//  Created by Anderson Homer on 7/23/13.
//
//

#import "AppDelegate.h"

@implementation AppDelegate
@synthesize tele_text;
@synthesize helptip_img_view;

@synthesize username_field;
@synthesize password_field;
@synthesize username_light,password_light;
@synthesize login_message;
@synthesize firstname,lastname,emailaddress,confirmemail,password,confirmpassword;
@synthesize quick_register_message;

-(void)textDidChange:(NSNotification *)notification {
    NSString *currtext = [[tele_text textStorage] string];
    NSLog(@"Text changed");
   
    NSBundle *mainbundle = [NSBundle mainBundle];
    
    NSString *myFile = [mainbundle pathForResource: @"vcf_first_load" ofType: @"txt"];
    [currtext writeToFile:myFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
  
}



- (BOOL)validateEmail:(NSString *)emailStr {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}



-(void)controlTextDidChange:(NSNotification *)notification {
   
    if (([username_field stringValue].length > 0 ) && ([password_field stringValue].length > 0 )) {
        [login_message setStringValue:@""];

    }
    
    if ([notification object] == username_field){
        
        if ([username_field stringValue].length > 0) {
            [username_light setHidden:NO];
            [username_light setImage:[NSImage imageNamed:@"step4_blue_image"]];
            
        }
        else
        {
            [username_light setHidden:NO];
            [username_light setImage:[NSImage imageNamed:@"step4_red_image"]];
            [login_message setStringValue:@"All fields are required"];
            
        }

        
        
        
    }
   
    if ([notification object] == password_field) {
       
        if ([password_field stringValue].length > 0) {
            [password_light setHidden:NO];
            [password_light setImage:[NSImage imageNamed:@"step4_blue_image"]];
        }
        else
        {
            [password_light setHidden:NO];
            [password_light setImage:[NSImage imageNamed:@"step4_red_image"]];
            [login_message setStringValue:@"All fields are required"];

        }

        
    }
    
   
    if ([notification object] == firstname) {
        
        if ([firstname stringValue].length < 2) {
            [quick_register_message setStringValue:@"Field must be at least 2 characters long"];
        }
        else
        {
            [quick_register_message setStringValue:@""];

        }
        
        
    }
    
    if ([notification object] == lastname) {
        
        if ([lastname stringValue].length < 2) {
            [quick_register_message setStringValue:@"Field must be at least 2 characters long"];
        }
        else
        {
            [quick_register_message setStringValue:@""];
            
        }
        
        
    }

    if ([notification object] == password) {
        
        if ([password stringValue].length < 7) {
            [quick_register_message setStringValue:@"Password must be at least 7 characters long"];
        }
        else
        {
            [quick_register_message setStringValue:@""];
            
        }
        
         
    }

    if ([notification object] == confirmpassword) {
        
        if (![[confirmpassword stringValue] isEqualToString:[password stringValue]]) {
            [quick_register_message setStringValue:@"Passwords must match"];
        }
        else
        {
            [quick_register_message setStringValue:@""];
            
        }
        
        
    }
    
    if ([notification object] == emailaddress) {
        
        if (![self validateEmail:[emailaddress stringValue]]) {
            [quick_register_message setStringValue:@"Email address must match format example@me.com"];
        }
        else
        {
            [quick_register_message setStringValue:@""];
            
        }
        
        
    }


    if ([notification object] == confirmemail) {
        
        if (![[confirmemail stringValue] isEqualToString:[emailaddress stringValue]]) {
            [quick_register_message setStringValue:@"Email addresses must match"];
        }
        else
        {
            [quick_register_message setStringValue:@""];
            
        }
        
        
    }

    
    
    
    
}




- (IBAction)quick_reg:(id)sender {
}
@end



