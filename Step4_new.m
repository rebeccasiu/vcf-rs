//
//  Step4_new.m
//  Videocareerfinder
//
//  Created by Anderson Homer on 7/25/13.
//
//

#import "GlobalVars.h"
#import "Step4_new.h"
#import <AppKit/AppKit.h>
#import "Controller.h"

@implementation Step4_new
@synthesize initialLocation;
@synthesize username;
@synthesize upassword;
@synthesize login_message;
@synthesize username_light;
@synthesize password_light;
@synthesize first_name,last_name,email_address,confirm_email_address,quick_password,quick_password_confirm;
@synthesize register_message;

/***********************************/

-(void) dealloc {
[super dealloc];
}



- (id)initWithContentRect:(NSRect)contentRect
				styleMask:(NSUInteger)windowStyle
				  backing:(NSBackingStoreType)bufferingType
					defer:(BOOL)deferCreation
{
	appStatus = @"4";
	
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    
	[self setLevel: NSPopUpMenuWindowLevel];
	
	[self center];
	
	[self setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"step4_background_image.png"]]];
	
	
	
    return self;
}

- (void)mouseDown:(NSEvent *)theEvent {
    // Get the mouse location in window coordinates.
    self.initialLocation = [theEvent locationInWindow];
}

- (BOOL)canBecomeKeyWindow {
    return YES;
}

- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect windowFrame = [self frame];
    NSPoint newOrigin = windowFrame.origin;
	
    // Get the mouse location in window coordinates.
    NSPoint currentLocation = [theEvent locationInWindow];
    // Update the origin with the difference between the new mouse location and the old mouse location.
    newOrigin.x += (currentLocation.x - initialLocation.x);
    newOrigin.y += (currentLocation.y - initialLocation.y);
	
    // Don't let window get dragged up under the menu bar
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height)) {
        newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    // Move the window to the new location
    [self setFrameOrigin:newOrigin];
}



- (IBAction)login_upload_btn:(id)sender {
 
    NSString  *currUsername = [username stringValue];
    NSString  *currPassword = [upassword stringValue];
    
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"username=%@&password=%@", currUsername, currPassword];
    
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    NSMutableURLRequest *request = [ [ NSMutableURLRequest alloc ] initWithURL: [ NSURL URLWithString: @"http://www.videocareerfinder.com/app/app_member_login" ] ];
    [ request setHTTPMethod: @"POST" ];
    [ request setHTTPBody: myRequestData ];
    [ request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
    
    NSData *dataReply = [ NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil ];
    
    NSString *stringReply = [[NSString alloc] initWithData:dataReply encoding:NSUTF8StringEncoding];
 
    NSLog(stringReply);
    
    NSXMLDocument *mydoc = [[NSXMLDocument alloc] initWithData:dataReply options:NSXMLDocumentTidyXML error:NULL];
    
    NSXMLElement  *rootElement = [mydoc rootElement];
    
    NSString *loginvalue = [[[rootElement elementsForName:@"login"] objectAtIndex:0] stringValue];
    NSString *loginmessage = [[[rootElement elementsForName:@"message"] objectAtIndex:0] stringValue];
    NSString *loginfirstname = [[[rootElement elementsForName:@"firstname"] objectAtIndex:0] stringValue];
    NSString *loginlastname = [[[rootElement elementsForName:@"lastname"] objectAtIndex:0] stringValue];
    
    //  NSString *scriptid = [[[element elementsForName:@"id"] objectAtIndex:0] stringValue];
    
    NSLog(loginvalue);
    
    
    
    
    if ([loginvalue isEqualToString:@"error"]) {
        [login_message setStringValue:loginmessage];
    }
    else{
        
        appFirstLastName = [loginfirstname stringByAppendingString:@" "];
        appFirstLastName = [appFirstLastName stringByAppendingString:loginlastname];
        NSLog(appFirstLastName);
        
        NSString *userfirstlast = appFirstLastName; // Just an example
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        if (standardUserDefaults) {
            [standardUserDefaults setObject:[NSString stringWithString:userfirstlast] forKey:@"fullname"];
            [standardUserDefaults setObject:[NSString stringWithString:currUsername] forKey:@"username"];
            [standardUserDefaults synchronize];
        }
        
        
        Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step5-new"];
        
        [myController showWindow:sender];
        [[myController window] makeKeyAndOrderFront:sender];
        [self close];
        [self release];
        
         
    }
     
       
}






- (BOOL)validateEmail:(NSString *)emailStr {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}


-(BOOL) isNotBlank:(NSString *)myfield {
    if (myfield.length == 0) {
        return false;
    }
    else
    {return true; }
    
}

- (IBAction)quick_reg:(id)sender {
    
    NSString *firstname = [first_name stringValue];
    NSString *lastname = [last_name stringValue];
    NSString *email = [email_address stringValue];
    NSString *confirmemail = [confirm_email_address stringValue];
    NSString *password = [quick_password stringValue];
    NSString *cpassword = [quick_password_confirm stringValue];
    
    BOOL success = false;
    
    if ([self isNotBlank:firstname] && [self isNotBlank:lastname] && [self isNotBlank:email] && [self isNotBlank:confirmemail] && [self isNotBlank:password] && [self isNotBlank:cpassword] && [self validateEmail:email] && [confirmemail isEqualToString:email] &&  (password.length > 6) &&  ([password isEqualToString:cpassword]))
    {
        success = true;
    }
    else{success = false;}
    
       
       
    if (success) {
      
        NSString *myRequestString = [[NSString alloc] initWithFormat:@"firstname=%@&lastname=%@&email=%@&email_confirm=%@&password=%@&password_confirm=%@", firstname,lastname,email,confirmemail,password,cpassword ];
        
        NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
        NSMutableURLRequest *request = [ [ NSMutableURLRequest alloc ] initWithURL: [ NSURL URLWithString: @"http://www.videocareerfinder.com/app/app_member_register" ] ];
        [ request setHTTPMethod: @"POST" ];
        [ request setHTTPBody: myRequestData ];
        [ request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
        
        NSData *dataReply = [ NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil ];
        
        NSString *stringReply = [[NSString alloc] initWithData:dataReply encoding:NSUTF8StringEncoding];
        
        NSLog(stringReply);
        
        NSXMLDocument *mydoc = [[NSXMLDocument alloc] initWithData:dataReply options:NSXMLDocumentTidyXML error:NULL];
        
        NSXMLElement  *rootElement = [mydoc rootElement];
        
        NSString *registervalue = [[[rootElement elementsForName:@"register"] objectAtIndex:0] stringValue];
        NSString *registermessage = [[[rootElement elementsForName:@"message"] objectAtIndex:0] stringValue];
        //  NSString *scriptid = [[[element elementsForName:@"id"] objectAtIndex:0] stringValue];
        
        NSLog(registervalue);

        NSLog(registermessage);
        
        
        [register_message setStringValue:registermessage];
        
        
        NSString *userfirstlast = [[firstname stringByAppendingString:@" "] stringByAppendingString:lastname]; // Just an example
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        if (standardUserDefaults) {
            [standardUserDefaults setObject:[NSString stringWithString:userfirstlast] forKey:@"fullname"];
            [standardUserDefaults setObject:[NSString stringWithString:email] forKey:@"username"];
            [standardUserDefaults synchronize];
        }
        
        
        
        
        Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step5-new"];
        
        [myController showWindow:sender];
        [[myController window] makeKeyAndOrderFront:sender];
        [self close];
        [self release];

        
        
        
    }
    else
    {
        [register_message setStringValue:@"All fields must be valid"];
    }
    
    
}
- (IBAction)back_btn:(id)sender {
    
    Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step3_new"];
    
    [myController showWindow:sender];
    [[myController window] makeKeyAndOrderFront:sender];
    [self close];
    [self release];
    

}
- (IBAction)close_app:(id)sender {
    [NSApp terminate: nil];

}
@end

