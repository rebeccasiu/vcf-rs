//
//  AgreeBtn.m
//  Videocareerfinder
//
//  Created by Anderson Homer on 7/17/13.
//
//

#import "AgreeBtn.h"

@implementation AgreeBtn

- (void)updateTrackingAreas
{
	[super updateTrackingAreas];
	
	if (trackingArea)
	{
		[self removeTrackingArea:trackingArea];
		[trackingArea release];
	}
	
	NSTrackingAreaOptions options = NSTrackingInVisibleRect | NSTrackingMouseEnteredAndExited | NSTrackingActiveInKeyWindow;
	trackingArea = [[NSTrackingArea alloc] initWithRect:NSZeroRect options:options owner:self userInfo:nil];
	[self addTrackingArea:trackingArea];
    
    truealtimage = [super alternateImage];
    trueimage = [super image];
    
}

- (void)mouseEntered:(NSEvent *)event
{
    [self setImage:truealtimage];
	//[self setImage:[NSImage imageNamed:@"agree_button_overlay"]];
   
}

- (void)mouseExited:(NSEvent *)event
{
	//[self setImage:[NSImage imageNamed:@"agree_button"]];
    [self setImage:trueimage];
}



@end
