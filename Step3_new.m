//
//  Untitled_AppDelegate.m
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import "GlobalVars.h"
#import "Step3_new.h"
#import <AppKit/AppKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Controller.h"
#import "NSFileManager+DirectoryLocations.h"


@implementation Step3_new
@synthesize play_btn;
@synthesize stop_btn;
@synthesize upload_btn;
@synthesize record_btn;
@synthesize speed_slider_val;
@synthesize helpimage;
@synthesize fontsize_slider_val;

@synthesize telescroller;
@synthesize mytele;
@synthesize initialLocation;
@synthesize isrecord;
QTMovie *newMovie;

- (void) dealloc{
   /* [mCaptureSession release];
    [[video device] release];
	[[audio device] release];
    [mCaptureMovieFileOutput release];
	
	[play_btn release];
	[stop_btn release];
	[record_btn release];
    [[NSNotificationCenter defaultCenter] removeObserver:self]; */
    [super dealloc];
    
	//[keepBtn release];
	
}


- (void)setButtonTitleFor:(NSButton*)button toString:(NSString*)title withColor:(NSColor*)color {
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSCenterTextAlignment];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                     color, NSForegroundColorAttributeName, style, NSParagraphStyleAttributeName, nil];
    NSAttributedString *attrString = [[NSAttributedString alloc]
                                      initWithString:title attributes:attrsDictionary];
    [button setAttributedTitle:attrString];
    [style release];
    [attrString release]; 
}



- (id)initWithContentRect:(NSRect)contentRect
				styleMask:(NSUInteger)windowStyle
				  backing:(NSBackingStoreType)bufferingType
					defer:(BOOL)deferCreation
{
	appStatus = @"2";
	
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    //[self orderOut:self];
	
	[self setLevel: NSPopUpMenuWindowLevel];
	
	[self center];
	
    [mytele setFont:[NSFont fontWithName:@"Arial" size:13]];
  //  [mytele setTextContainerInset:NSMakeSize(30, 5)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myTestNotificationReceived:)
                                                 name:@"myTestNotification"
                                               object:nil];
    
    return self;
}

-(void)awakeFromNib {
    NSBundle *mainbundle = [NSBundle mainBundle];
    NSString *myFile = [mainbundle pathForResource: @"vcf_first_load" ofType: @"txt"];
    NSString *filestring = [NSString stringWithContentsOfFile:myFile encoding:NSUTF8StringEncoding error:nil];
    [mytele setString:[GlobalVars getScript]];
    [fontsize_slider_val setStringValue:[GlobalVars getFont]];
    [self.font_label setStringValue:[fontsize_slider_val stringValue]];
    [speed_slider_val setStringValue:[GlobalVars getSpeed]];
    [self.speed_label setStringValue:[speed_slider_val stringValue]];
    [self update_play_button_state];

    NSFileManager *manager = [NSFileManager defaultManager];

    if (![manager fileExistsAtPath:[self getMovieFilePath]]) {
        [play_btn setEnabled:FALSE];
    }
    [stop_btn setEnabled:FALSE];

    [self startCam];
}

-(void) setImageHelpTip:(NSString *)image_name {
    helpimage.hidden = false;
    [helpimage setImage:[NSImage imageNamed:image_name]];
    
}

- (void) myTestNotificationReceived:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"myTestNotification"]){
        NSString *sendingimage = (NSString *) [notification object];
     //     NSLog (@"Notification is successfully received!");
      //  NSLog(sendingimage);
        if (![sendingimage  isEqual: @"hide"] )
        [self setImageHelpTip:sendingimage];
        else
            helpimage.hidden = true;   }
      
      
}



- (void)mouseDown:(NSEvent *)theEvent {
    // Get the mouse location in window coordinates.
    self.initialLocation = [theEvent locationInWindow];
}

- (BOOL)canBecomeKeyWindow {
    return YES;
}




- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect windowFrame = [self frame];
    NSPoint newOrigin = windowFrame.origin;
	
    // Get the mouse location in window coordinates.
    NSPoint currentLocation = [theEvent locationInWindow];
    // Update the origin with the difference between the new mouse location and the old mouse location.
    newOrigin.x += (currentLocation.x - initialLocation.x);
    newOrigin.y += (currentLocation.y - initialLocation.y);
	
    // Don't let window get dragged up under the menu bar
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height)) {
        newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    // Move the window to the new location
    [self setFrameOrigin:newOrigin];
}



- (void) doAnimation:(float) speedValue {
    self.is_teleprompter_scrolling = YES;
    float timeToAnimate = (speed_slider_val.intValue - 40) * -1.5;
    
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:timeToAnimate];
    [[NSAnimationContext currentContext] setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
	NSClipView* clipView = [[mytele enclosingScrollView] contentView];
	NSPoint startpoint = [clipView bounds].origin;
    startpoint.y = 0;
    [clipView setBoundsOrigin:startpoint];
    NSPoint newOrigin = [clipView bounds].origin;
    newOrigin.y = [mytele frame].size.height;
	[[clipView animator] setBoundsOrigin:newOrigin];
	[NSAnimationContext endGrouping];
    
    
    
    
}


- (IBAction)left_align_btn:(id)sender {
    [mytele alignLeft:sender];
}

- (IBAction)center_align_btn:(id)sender {
    [mytele alignCenter:sender];
}

- (IBAction)black_on_white_btn:(id)sender {
    mytele.backgroundColor = [NSColor whiteColor];
    mytele.textColor = [NSColor blackColor];
    [mytele setInsertionPointColor:[NSColor blackColor]];
   
 //   [mytele setFont:[NSFont fontWithName:@"Courier" size:20]];
}

- (IBAction)white_on_black_btn:(id)sender {
    
    mytele.backgroundColor = [NSColor blackColor];
    mytele.textColor = [NSColor whiteColor];
    [mytele setInsertionPointColor:[NSColor whiteColor]];
}

- (IBAction)fontsize_slider:(id)sender {
    [fontsize_slider_val setIntValue:round(fontsize_slider_val.doubleValue)];
    [self.font_label setStringValue:[fontsize_slider_val stringValue]];
    [mytele setFont:[NSFont fontWithName:@"Arial" size:[fontsize_slider_val intValue]]];
}

- (IBAction)toggleTeleprompter:(id)sender
{
    // if not scrolling, start scroll
    if (!self.is_teleprompter_scrolling) {
        [self doAnimation:0.0];
        [self.teleprompter_btn setTitle:@"Stop Teleprompter"];

    } else {
        [self stoptelescroll];
        [self.teleprompter_btn setTitle:@"Start Teleprompter"];
    }
}

- (IBAction)speed_slider:(id)sender {
    [speed_slider_val setIntValue:round(speed_slider_val.doubleValue)];
    [self.speed_label setStringValue:[speed_slider_val stringValue]];
}


-(void) stoptelescroll {
    self.is_teleprompter_scrolling = NO;
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:0.0f];
    [[NSAnimationContext currentContext] setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
	NSClipView* clipView = [[mytele enclosingScrollView] contentView];
    NSPoint newOrigin = [clipView bounds].origin;
    newOrigin.y = 0;
	[[clipView animator] setBoundsOrigin:newOrigin];
	[NSAnimationContext endGrouping];
   
    
}

-(void) startCam {

    mMovieView.hidden = false;
    mCaptureView.hidden = false;
    record_btn.hidden = false;
    play_btn.hidden = false;
    stop_btn.hidden = false;
    upload_btn.hidden = false;
    
    mCaptureSession = [[QTCaptureSession alloc] init];
	BOOL success = NO;
    NSError *error;
	
	QTCaptureDevice *videoDevice = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeVideo];
	success = [videoDevice open:&error];
	
	QTCaptureDevice *audioDevice = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeSound];
	success = [audioDevice open:&error];
	
	
	
    if (videoDevice && audioDevice) {
        success = [videoDevice open:&error];
        if (!success) {
        }
		
		success = [audioDevice open:&error];
        if (!success) {
        }
        
		video = [[QTCaptureDeviceInput alloc] initWithDevice:videoDevice];
		success = [mCaptureSession addInput:video error:&error];
		
		audio = [[QTCaptureDeviceInput alloc] initWithDevice:audioDevice];
		success = [mCaptureSession addInput:audio error:&error];
		
		
        if (!success) {
            // Handle error
        }
		
		mCaptureMovieFileOutput = [[QTCaptureMovieFileOutput alloc] init];
		success = [mCaptureSession addOutput:mCaptureMovieFileOutput error:&error];
		if (!success) {
		}
		[mCaptureMovieFileOutput setDelegate:self];
		
		NSEnumerator *connectionEnumerator = [[mCaptureMovieFileOutput connections] objectEnumerator];
        QTCaptureConnection *connection;
		
		while ((connection = [connectionEnumerator nextObject])) {
            NSString *mediaType = [connection mediaType];
            QTCompressionOptions *compressionOptions = nil;
            if ([mediaType isEqualToString:QTMediaTypeVideo]) {
                compressionOptions = [QTCompressionOptions compressionOptionsWithIdentifier:@"QTCompressionOptionsJPEGVideo"];
            } else if ([mediaType isEqualToString:QTMediaTypeSound]) {
                compressionOptions = [QTCompressionOptions compressionOptionsWithIdentifier:@"QTCompressionOptionsHighQualityAACAudio"];
            }
			
            [mCaptureMovieFileOutput setCompressionOptions:compressionOptions forConnection:connection];
			[mCaptureView setCaptureSession:mCaptureSession];
		}
		
		
		[mCaptureSession startRunning];
		//startCountdown();
	}

    
}

- (NSString *)getMovieFilePath
{
    return [NSString stringWithFormat:@"%@/vcf-%ld.mov", [[NSFileManager defaultManager] applicationSupportDirectory], (long)self.movie_segment.selectedSegment];
}

- (IBAction)movie_segment_changed:(id)sender
{
    // TODO: if movie isn't playing or being recorded, or just disable segmented control after play / record
    [self update_play_button_state];
}

- (void)update_play_button_state
{
    NSFileManager *manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:[self getMovieFilePath]]) {
        [play_btn setEnabled:FALSE];
    } else {
        [play_btn setEnabled:TRUE];
    }
}

- (IBAction)record_btn_press:(id)sender {
    
    isrecord = 1;
	//pauseTimer = 0;
	
    NSLog(@"STARTING RECORDING");
	[mCaptureMovieFileOutput recordToOutputFileURL:[NSURL fileURLWithPath:[self getMovieFilePath]]];
	counter = 0;
	
	
	[play_btn setEnabled:FALSE];
	[record_btn setEnabled:FALSE];
//	[keepBtn setEnabled:FALSE];
	[stop_btn setEnabled:TRUE];
    
    //Show teleprompter
   
    float currspeed = [speed_slider_val floatValue];
    [self doAnimation:currspeed];
    
    
}




- (void)movieLoadStateDidChange:(NSNotification *)notification
{
	// First make sure that this notification is for our movie.
	
    if([newMovie rate] == 0)
    {
        newMovie = nil;
        [mMovieView setMovie:newMovie];
                
        NSLog(@"STARTING CAMERA");
        [mCaptureView setHidden:NO];
        [play_btn setEnabled:TRUE];
        [record_btn setEnabled:TRUE];
      //  [keepBtn setEnabled:TRUE];
        [self startCam];
      
    }
}	



- (IBAction)play_btn_press:(id)sender {
    NSLog(@"PLAYING MOVIE");
    isrecord = 0;
	[mCaptureSession stopRunning];
    [[video device] close];
	[[audio device] close];
	[mCaptureView setHidden:YES];
	
	[play_btn setEnabled:FALSE];
	[record_btn setEnabled:FALSE];
	
/*
	//sIMULATE movie playing
    
    NSLog(@"STARTING CAMERA");
    [mCaptureView setHidden:NO];
    [play_btn setEnabled:TRUE];
    [record_btn setEnabled:TRUE];
    //  [keepBtn setEnabled:TRUE];
    [self startCam];
    
 */
    
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(movieLoadStateDidChange:)
												 name:QTMovieRateDidChangeNotification
											   object:nil];
  
    
    
	newMovie = [QTMovie movieWithURL:[NSURL fileURLWithPath:[self getMovieFilePath]] error:nil];
	[mMovieView setMovie:newMovie];
	[newMovie play];
    
    
}


- (IBAction)stop_btn_press:(id)sender {
    [self stoptelescroll];
    
   
    
    
          if(isrecord==1)
	{
        NSLog(@"RECORD=1 ENDING RECORDING");
		//pauseTimer = 1;
		counter = 0;
		[mCaptureMovieFileOutput recordToOutputFileURL:nil];
		[play_btn setEnabled:TRUE];
		[record_btn setEnabled:TRUE];
		//[keepBtn setEnabled:TRUE];
		[stop_btn setEnabled:FALSE];
        	}
	else if(isrecord==0)
	{
         NSLog(@"RECORD=0");
     //   [mCaptureMovieFileOutput recordToOutputFileURL:nil];
      	[mMovieView setMovie:nil];
		[mCaptureView setHidden:NO];
		[self startCam];
		[play_btn setEnabled:TRUE];
		[record_btn setEnabled:TRUE];
		//[keepBtn setEnabled:TRUE];
		[stop_btn setEnabled:FALSE];
       
	}
    
    
}

- (IBAction)upload_btn_press:(id)sender {
    
    [mCaptureSession stopRunning];
    [[video device] close];
	[[audio device] close];
        
    Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step5-new"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];
    
}







- (IBAction)close_app:(id)sender {
    [NSApp terminate: nil];

}
@end
