//
//  HoverButton.m
//  HoverButton
//

#import "RecordBtn.h"


@implementation RecordBtn

- (void)updateTrackingAreas
{
	[super updateTrackingAreas];
	
	if (trackingArea)
	{
		[self removeTrackingArea:trackingArea];
		[trackingArea release];
	}
	
	NSTrackingAreaOptions options = NSTrackingInVisibleRect | NSTrackingMouseEnteredAndExited | NSTrackingActiveInKeyWindow;
	trackingArea = [[NSTrackingArea alloc] initWithRect:NSZeroRect options:options owner:self userInfo:nil];
	[self addTrackingArea:trackingArea];
}

- (void)mouseEntered:(NSEvent *)event
{
	[self setImage:[NSImage imageNamed:@"record_btn_hover"]];
}

- (void)mouseExited:(NSEvent *)event
{
	[self setImage:[NSImage imageNamed:@"record_btn"]];
}

@end