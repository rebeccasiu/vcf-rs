//
//  Untitled_AppDelegate.m
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import "GlobalVars.h"
#import "Step2.h"
#import <AppKit/AppKit.h>
#import "Controller.h"

@implementation Step2

@synthesize initialLocation;






- (IBAction)proc_tutorial:(id)sender {

    NSLog(@"click");
    
    Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step3_tutorial"];
	
    [myController showWindow:sender];
    [[myController window] makeKeyAndOrderFront:sender];
     [self close];
    [self release];
    
    
}

- (IBAction)proc_without_tutorial:(id)sender {
    
    Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step3_new"];
	
    [myController showWindow:sender];
    [[myController window] makeKeyAndOrderFront:sender];
    [self close];
    [self release];

    
}

- (IBAction)back_to_Step2:(id)sender {
}

-(IBAction)closeApp:(id)sender
{
	[NSApp terminate: nil];

}

- (void)dealloc
{
	
	//[chk_no dealloc];
	//[chk_yes dealloc];
	[super dealloc] ;
}


/**********************************************/




- (id)initWithContentRect:(NSRect)contentRect
				styleMask:(NSUInteger)windowStyle
				  backing:(NSBackingStoreType)bufferingType
					defer:(BOOL)deferCreation
{
	appStatus = @"2";
	
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    //[self orderOut:self];
	
	[self setLevel: NSPopUpMenuWindowLevel];
	
	[self center];
	
	[self setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"step1_welcome_new.png"]]];
	
	
    return self;	
}


- (void)mouseDown:(NSEvent *)theEvent {    
    // Get the mouse location in window coordinates.
    self.initialLocation = [theEvent locationInWindow];
}

- (BOOL)canBecomeKeyWindow {
    return YES;
}



- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect windowFrame = [self frame];
    NSPoint newOrigin = windowFrame.origin;
	
    // Get the mouse location in window coordinates.
    NSPoint currentLocation = [theEvent locationInWindow];
    // Update the origin with the difference between the new mouse location and the old mouse location.
    newOrigin.x += (currentLocation.x - initialLocation.x);
    newOrigin.y += (currentLocation.y - initialLocation.y);
	
    // Don't let window get dragged up under the menu bar
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height)) {
        newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    // Move the window to the new location
    [self setFrameOrigin:newOrigin];
}




@end