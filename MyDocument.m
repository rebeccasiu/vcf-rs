//
//  MyDocument.m
//  Untitled1
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import "MyDocument.h"
#import "Controller.h"

@implementation MyDocument

- (id)init 
{
    self = [super init];
    if (self != nil) {
        // initialization code
    }
    return self;
}

- (void)makeWindowControllers
{
    Controller *controller = [[Controller alloc] init];
    [controller autorelease];
    [self addWindowController:controller];
}



- (void)windowControllerDidLoadNib:(NSWindowController *)windowController 
{
    [super windowControllerDidLoadNib:windowController];
    // user interface preparation code
}


@end
