//
//  AlternateImageBtn.h
//  Videocareerfinder
//
//  Created by Anderson Homer on 7/24/13.
//
//

#import <Cocoa/Cocoa.h>

@interface AlternateImageBtn : NSButton
{
	NSTrackingArea *trackingArea;
    NSImage *trueimage;
    NSImage *truealtimage;
    }

@end
