#import "GlobalVars.h"
#import "Login.h"
#import "Controller.h"

@interface Login ()

@property (assign) IBOutlet NSTextField *username;
@property (assign) IBOutlet NSSecureTextField *password;
@property (assign) IBOutlet NSTextField *validation;

@end

@implementation Login

@synthesize username;
@synthesize password;
@synthesize validation;

- (IBAction)login:(id)sender
{
    if ([self.username stringValue].length < 6 || [self.password stringValue].length < 6) {
        if ([self.username stringValue].length == 0) {
            [self.validation setStringValue:@"Username is a required field"];
        } else if ([self.username stringValue].length < 6) {
            [self.validation setStringValue:@"Username must be at least 6 characters"];
        } else if ([self.password stringValue].length == 0) {
            [self.validation setStringValue:@"Password is a required field"];
        } else if ([self.password stringValue].length < 6) {
            [self.validation setStringValue:@"Password must be at least 6 characters"];
        }
        return;
    }
    
    NSString *myRequestString = [[[NSString alloc] initWithFormat:@"username=%@&password=%@", self.username.stringValue, self.password.stringValue] autorelease];
    
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    NSMutableURLRequest *request = [ [ NSMutableURLRequest alloc ] initWithURL: [ NSURL URLWithString: @"http://portal.videocareerfinder.com/app/app_member_login" ] ];
    [ request setHTTPMethod: @"POST" ];
    [ request setHTTPBody: myRequestData ];
    [ request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
    
    NSData *dataReply = [ NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil ];
    
    NSXMLDocument *mydoc = [[NSXMLDocument alloc] initWithData:dataReply options:NSXMLDocumentTidyXML error:NULL];
    
    NSXMLElement  *rootElement = [mydoc rootElement];
    
    NSString *loginvalue = [[[rootElement elementsForName:@"login"] objectAtIndex:0] stringValue];
    NSString *loginmessage = [[[rootElement elementsForName:@"message"] objectAtIndex:0] stringValue];

    if ([loginvalue isEqualToString:@"error"]) {
        [self.validation setStringValue:loginmessage];
    } else {
        NSString *firstname = [[[rootElement elementsForName:@"firstname"] objectAtIndex:0] stringValue];
        NSString *lastname = [[[rootElement elementsForName:@"lastname"] objectAtIndex:0] stringValue];
        NSString *script = [[[rootElement elementsForName:@"script"] objectAtIndex:0] stringValue];
        NSString *alignment = [[[rootElement elementsForName:@"alignment"] objectAtIndex:0] stringValue];
        NSString *color = [[[rootElement elementsForName:@"color"] objectAtIndex:0] stringValue];
        NSString *font = [[[rootElement elementsForName:@"font"] objectAtIndex:0] stringValue];
        NSString *speed = [[[rootElement elementsForName:@"speed"] objectAtIndex:0] stringValue];
        
        [GlobalVars setFirstName:firstname];
        [GlobalVars setLastName:lastname];
        [GlobalVars setScript:script];
        [GlobalVars setAlignment:alignment];
        [GlobalVars setColor:color];
        [GlobalVars setFont:font];
        [GlobalVars setSpeed:speed];

        // todo: check if we really need this
        appFirstLastName = [NSString stringWithFormat:@"%@ %@", firstname, lastname];
        
        Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step3_new"];
        
        [myController showWindow:sender];
        [[myController window] makeKeyAndOrderFront:sender];
        [self close];
        [self release];
    }
}

- (void)controlTextDidChange:(NSNotification *)notification
{
    if ([notification object] == self.username) {
        if ([self.username stringValue].length == 0) {
            [self.validation setStringValue:@"Username is a required field"];
        } else if ([self.username stringValue].length < 6) {
            [self.validation setStringValue:@"Username must be at least 6 characters"];
        } else {
            [self.validation setStringValue:@""];
        }
    }

    if ([notification object] == self.password) {
        if ([self.password stringValue].length == 0) {
            [self.validation setStringValue:@"Password is a required field"];
        } else if ([self.password stringValue].length < 6) {
            [self.validation setStringValue:@"Password must be at least 6 characters"];
        } else {
            [self.validation setStringValue:@""];
        }
    }
}

@end
