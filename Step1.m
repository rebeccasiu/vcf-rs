//
//  Untitled_AppDelegate.m
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import "GlobalVars.h"
#import "Step1.h"
#import <AppKit/AppKit.h>
#import "Controller.h"

@implementation Step1
@synthesize tac_button;



@synthesize initialLocation;
@synthesize next_btn;
@synthesize disagree_btn;
@synthesize agree_btn;
@synthesize sorryclose_btn;
@synthesize backtostep1_btn;
@synthesize sorry_image_overlay;


-(IBAction)closeApp:(id)sender;
{
	[NSApp terminate: nil];
}

- (IBAction)show_tac:(id)sender {
    mywebview.hidden = false;
    NSString *urlAddress = @"http://www.videocareerfinder.com/condition.html";
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [[mywebview mainFrame] loadRequest:requestObj];
    
    
}

- (IBAction)show_priv:(id)sender {
     mywebview.hidden = false;
    NSString *urlAddress = @"http://www.videocareerfinder.com/privacy.html";
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [[mywebview mainFrame] loadRequest:requestObj];

}

- (IBAction)goto_step2:(id)sender {
    
    
	Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step2"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];
}



- (void)dealloc
{
	[next_btn dealloc];
	[super dealloc] ;
}



/***********************************/

- (id)initWithContentRect:(NSRect)contentRect
				styleMask:(NSUInteger)windowStyle
				  backing:(NSBackingStoreType)bufferingType
					defer:(BOOL)deferCreation
{
	appStatus = @"1";
	
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    
	[self setLevel: NSPopUpMenuWindowLevel];	
	
	[self center];
  
	
	[self setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"step1_welcome_border.png"]]];
	
    
	[self makeKeyAndOrderFront:nil];
   
    return self;	
}



- (BOOL) canBecomeKeyWindow 
{
    return YES;
}





- (void)mouseDown:(NSEvent *)theEvent {    
    // Get the mouse location in window coordinates.
    self.initialLocation = [theEvent locationInWindow];
}


- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect windowFrame = [self frame];
    NSPoint newOrigin = windowFrame.origin;
	
    // Get the mouse location in window coordinates.
    NSPoint currentLocation = [theEvent locationInWindow];
    // Update the origin with the difference between the new mouse location and the old mouse location.
    newOrigin.x += (currentLocation.x - initialLocation.x);
    newOrigin.y += (currentLocation.y - initialLocation.y);
	
    // Don't let window get dragged up under the menu bar
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height)) {
        newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    // Move the window to the new location
    [self setFrameOrigin:newOrigin];
}



- (IBAction)back_btn:(id)sender {
}


- (IBAction)backtostep1:(id)sender {
    [sorry_image_overlay setHidden:YES];
   [sorryclose_btn setHidden:YES];
    [backtostep1_btn setHidden:YES];

}

- (IBAction)sorryclose:(id)sender {
 [NSApp terminate: nil];   
}

- (IBAction)disagree:(id)sender {
    [sorry_image_overlay setHidden:NO];
    [sorryclose_btn setHidden:NO];
    [backtostep1_btn setHidden:NO];
     
}
@end
