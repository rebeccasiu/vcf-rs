//
//  Untitled_AppDelegate.m
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import "GlobalVars.h"
#import "Step8_new.h"
#import <AppKit/AppKit.h>
#import "Controller.h"

@implementation Step8_new

@synthesize initialLocation;







-(IBAction)closeApp:(id)sender
{
	Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step11"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];
}

- (IBAction)next_step:(id)sender {
    NSURL *myurl = [NSURL URLWithString:@"http://www.videocareerfinder.com/candidate"];
    [[NSWorkspace sharedWorkspace] openURL:myurl];
    [NSApp terminate:sender];
}

- (IBAction)close_app:(id)sender {
    [NSApp terminate: nil];

}

- (void)dealloc
{
	
	//[chk_no dealloc];
	//[chk_yes dealloc];
	[super dealloc] ;
}


/**********************************************/




- (id)initWithContentRect:(NSRect)contentRect
				styleMask:(NSUInteger)windowStyle
				  backing:(NSBackingStoreType)bufferingType
					defer:(BOOL)deferCreation
{
	appStatus = @"8";
	
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    //[self orderOut:self];
	
	[self setLevel: NSPopUpMenuWindowLevel];
	
	[self center];
	
	[self setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"step8_background.png"]]];
	
	
    return self;	
}


- (void)mouseDown:(NSEvent *)theEvent {    
    // Get the mouse location in window coordinates.
    self.initialLocation = [theEvent locationInWindow];
}

- (BOOL)canBecomeKeyWindow {
    return YES;
}



- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect windowFrame = [self frame];
    NSPoint newOrigin = windowFrame.origin;
	
    // Get the mouse location in window coordinates.
    NSPoint currentLocation = [theEvent locationInWindow];
    // Update the origin with the difference between the new mouse location and the old mouse location.
    newOrigin.x += (currentLocation.x - initialLocation.x);
    newOrigin.y += (currentLocation.y - initialLocation.y);
	
    // Don't let window get dragged up under the menu bar
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height)) {
        newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    // Move the window to the new location
    [self setFrameOrigin:newOrigin];
}




@end