//
//  Untitled_AppDelegate.h
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QTKit/QTKit.h>

@interface Step10 : NSWindow 
{
	NSPoint initialLocation;
    
    IBOutlet QTMovieView *mMovieView;
    NSButton *close_btn;
    NSButton *continue_btn;
    NSButton *record_btn;
    NSButton *playprev_btn;
    NSTextField *name_label;
}

@property (assign) NSPoint initialLocation;
-(IBAction)closeApp:(id)sender;
- (IBAction)playpreview:(id)sender;

@property (assign) IBOutlet NSButton *playprev_btn;

- (IBAction)record_again:(id)sender;
- (IBAction)continue_uploading:(id)sender;
- (IBAction)close_preview:(id)sender;
@property (assign) IBOutlet NSButton *close_btn;
@property (assign) IBOutlet NSButton *continue_btn;
@property (assign) IBOutlet NSButton *record_btn;

@property (assign) IBOutlet NSTextField *name_label;
- (IBAction)close_app:(id)sender;

- (IBAction)back_btn:(id)sender;
@end
