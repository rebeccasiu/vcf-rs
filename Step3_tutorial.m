//
//  Untitled_AppDelegate.m
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import "GlobalVars.h"
#import "Step3_tutorial.h"
#import <AppKit/AppKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Controller.h"
#import "NSFileManager+DirectoryLocations.h"


@implementation Step3_tutorial
@synthesize play_btn;
@synthesize stop_btn;
@synthesize upload_btn;
@synthesize record_btn;
@synthesize stop_test_Scroll_btn;
@synthesize speed_slider_val;
@synthesize helpimage;
@synthesize fontsize_slider_val;

@synthesize telescroller;
@synthesize mytele;
@synthesize mywebview;
@synthesize initialLocation;
@synthesize isrecord;
@synthesize update_Scripts_btn,panel2_header,panel2_candidate_btn,panel2_grad_btn,panel2_military_btn,panel2_student_btn,panel2_script_header;
@synthesize script10_btn,script1_btn,script2_btn,script3_btn,script4_btn,script5_btn,script6_btn,script7_btn,script8_btn,script9_btn;
@synthesize panel3_header,panel3_left_btn,panel3_center_btn,panel3_Black,panel3_white,panel3_manual_Scroll_btn,panel3_scroll_button,panel3_scroll_header,panel4_record_btn;
@synthesize panel2_next2,next1_btn,panel3_next3,panel4_next4;

QTMovie *newMovie;

-(void) hidePanels {
   
    //Panel 2
    
    [panel2_header setHidden:YES];
    [panel2_candidate_btn setHidden:YES];
    [panel2_grad_btn setHidden:YES];
    [panel2_military_btn setHidden:YES];
    [panel2_student_btn setHidden:YES];
    [panel2_script_header setHidden:YES];
    [script1_btn setHidden:YES];
    [script2_btn setHidden:YES];
    [script3_btn setHidden:YES];
    [script4_btn setHidden:YES];
    [script5_btn setHidden:YES];
    [script6_btn setHidden:YES];
    [script7_btn setHidden:YES];
    [script8_btn setHidden:YES];
    [script9_btn setHidden:YES];
    [script10_btn setHidden:YES];
    [update_Scripts_btn setHidden:YES];
    [panel2_next2 setHidden:YES];
    
    
    //Panel 3
    
    [panel3_header setHidden:YES];
    [panel3_left_btn setHidden:YES];
    [panel3_center_btn setHidden:YES];
    [panel3_white setHidden:YES];
    [panel3_Black setHidden:YES];
    [panel3_scroll_header setHidden:YES];
    [panel3_scroll_button setHidden:YES];
    [stop_test_Scroll_btn setHidden:YES];
    [panel3_manual_Scroll_btn setHidden:YES];
    [fontsize_slider_val setHidden:YES];
    [speed_slider_val setHidden:YES];
    [panel3_next3 setHidden:YES];
    
    
    //Panel 4
    
    [panel4_record_btn setHidden:YES];
    [panel4_next4 setHidden:YES];
    
}

-(void) showPanel2 {
    [panel2_header setHidden:NO];
    [panel2_candidate_btn setHidden:NO];
    [panel2_grad_btn setHidden:NO];
    [panel2_military_btn setHidden:NO];
    [panel2_student_btn setHidden:NO];
     
}

-(void) showPanel2_lower {
    
    [panel2_script_header setHidden:NO];
    [script1_btn setHidden:NO];
    [script2_btn setHidden:NO];
    [script3_btn setHidden:NO];
    [script4_btn setHidden:NO];
    [script5_btn setHidden:NO];
    [script6_btn setHidden:NO];
    [script7_btn setHidden:NO];
    [script8_btn setHidden:NO];
    [script9_btn setHidden:NO];
    [script10_btn setHidden:NO];
    [update_Scripts_btn setHidden:NO];
    [panel2_next2 setHidden:NO];

}


-(void) showPanel3 {
    [panel3_header setHidden:NO];
    [panel3_left_btn setHidden:NO];
    [panel3_center_btn setHidden:NO];
    [panel3_white setHidden:NO];
    [panel3_Black setHidden:NO];
    [panel3_scroll_header setHidden:NO];
    [panel3_scroll_button setHidden:NO];
    [stop_test_Scroll_btn setHidden:NO];
    [panel3_manual_Scroll_btn setHidden:NO];
    [fontsize_slider_val setHidden:NO];
    [speed_slider_val setHidden:NO];
    [panel3_next3 setHidden:NO];
  
    
}


-(void)showPanel4 {
    [panel4_record_btn setHidden:NO];
    [panel4_next4 setHidden:NO];
    

}

- (void) dealloc{
   /* [mCaptureSession release];
    [[video device] release];
	[[audio device] release];
    [mCaptureMovieFileOutput release];
	
	[play_btn release];
	[stop_btn release];
	[record_btn release];
    [[NSNotificationCenter defaultCenter] removeObserver:self]; */
    [super dealloc];
    
	//[keepBtn release];
	
}

- (void)setButtonTitleFor:(NSButton*)button toString:(NSString*)title withColor:(NSColor*)color {
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSCenterTextAlignment];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                     color, NSForegroundColorAttributeName, style, NSParagraphStyleAttributeName, nil];
    NSAttributedString *attrString = [[NSAttributedString alloc]
                                      initWithString:title attributes:attrsDictionary];
    [button setAttributedTitle:attrString];
    [style release];
    [attrString release];
}



- (id)initWithContentRect:(NSRect)contentRect
				styleMask:(NSUInteger)windowStyle
				  backing:(NSBackingStoreType)bufferingType
					defer:(BOOL)deferCreation
{
	appStatus = @"2";
	
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    //[self orderOut:self];
	
	[self setLevel: NSPopUpMenuWindowLevel];
	
	[self center];
	
	[self setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"step2_background.png"]]];
	
    candidatescript_array = [[NSMutableArray alloc] init];
    studentscript_array = [[NSMutableArray alloc] init];
    recentgradscript_array = [[NSMutableArray alloc] init];
    militaryscript_array = [[NSMutableArray alloc] init];
    
    [mytele setFont:[NSFont fontWithName:@"Courier" size:13]];
   
    
    [self loadScriptsfromXML];
    myglob = @"Candidate";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myTestNotificationReceived:)
                                                 name:@"myTestNotification"
                                               object:nil];
    
    return self;
}

-(void)awakeFromNib {
    [self hideVideoWindows];
    [self hidePanels];
    [self setButtonTitleFor:panel3_manual_Scroll_btn toString:@"Manual scroll" withColor:[NSColor whiteColor]];
}

-(void) setImageHelpTip:(NSString *)image_name {
    helpimage.hidden = false;
    [helpimage setImage:[NSImage imageNamed:image_name]];
    
}

- (void) myTestNotificationReceived:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"myTestNotification"]){
        NSString *sendingimage = (NSString *) [notification object];
     //     NSLog (@"Notification is successfully received!");
      //  NSLog(sendingimage);
        if (![sendingimage  isEqual: @"hide"] )
        [self setImageHelpTip:sendingimage];
        else
            helpimage.hidden = true;   }
      
      
}

-(void)displayLastSavedScript {
    
    NSBundle *mainbundle = [NSBundle mainBundle];
    NSString *myFile = [mainbundle pathForResource: @"vcf_first_load" ofType: @"txt"];
    NSString *filestring = [NSString stringWithContentsOfFile:myFile encoding:NSUTF8StringEncoding error:nil];
    NSLog(filestring);
    [self showTelePrompter];
    [mytele setString:myglob];
}

-(void) loadScriptsfromXML {
  
    
    //Read local file into string
    NSBundle *mainbundle = [NSBundle mainBundle];
    NSString *myXMLFile = [mainbundle pathForResource: @"vcf-scripts" ofType: @"xml"];
    NSString *XMLfilestring = [NSString stringWithContentsOfFile:myXMLFile encoding:NSUTF8StringEncoding error:nil];
    
    
       NSXMLDocument *mydoc = [[NSXMLDocument alloc] initWithXMLString:XMLfilestring options:NSXMLDocumentTidyXML error:NULL];
    
    NSXMLElement  *rootElement = [mydoc rootElement];
    
    //Pointer to all scripts
    NSArray *userscripts = [rootElement elementsForName:@"script"];
    
  
    //Populate arrays
    
    for (int i=0; i<[userscripts count]; i++)
    {
        
       
             
        
        NSXMLElement *element = [userscripts objectAtIndex:i];
        NSString *scriptid = [[[element elementsForName:@"id"] objectAtIndex:0] stringValue];
        NSString *script_text = [[[element elementsForName:@"text"] objectAtIndex:0] stringValue];
        
       if ([scriptid rangeOfString:@"Student"].location != NSNotFound) {
            [studentscript_array addObject:script_text];
            
        } 
        
        if ([scriptid rangeOfString:@"Candidate"].location != NSNotFound) {
            [candidatescript_array addObject:script_text];
            
        }
     
        
        if ([scriptid rangeOfString:@"Graduate"].location != NSNotFound) {
            [recentgradscript_array addObject:script_text];
            
        }
        
        if ([scriptid rangeOfString:@"Military"].location != NSNotFound) {
            [militaryscript_array addObject:script_text];
            
        }
        
        
    }
    
   
}



- (void)mouseDown:(NSEvent *)theEvent {
    // Get the mouse location in window coordinates.
    self.initialLocation = [theEvent locationInWindow];
}

- (BOOL)canBecomeKeyWindow {
    return YES;
}




- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect windowFrame = [self frame];
    NSPoint newOrigin = windowFrame.origin;
	
    // Get the mouse location in window coordinates.
    NSPoint currentLocation = [theEvent locationInWindow];
    // Update the origin with the difference between the new mouse location and the old mouse location.
    newOrigin.x += (currentLocation.x - initialLocation.x);
    newOrigin.y += (currentLocation.y - initialLocation.y);
	
    // Don't let window get dragged up under the menu bar
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height)) {
        newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    // Move the window to the new location
    [self setFrameOrigin:newOrigin];
}


-(void) loadPageinWebView:(NSString *)mywebpage {
  
    
    telescroller.hidden = true;
    mywebview.hidden = false;
    NSString *urlAddress = mywebpage;
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [[mywebview mainFrame] loadRequest:requestObj];
 
    
    
}







- (IBAction)practice_btn:(id)sender {
   
    [self loadPageinWebView:@"http://www.videocareerfinder.com/app/practice_makes_perfect"];
  
}

- (IBAction)howdone_btn:(id)sender {
     [self loadPageinWebView:@"http://www.videocareerfinder.com/app/how_is_it_done"];
}

- (IBAction)teleprompt_btn:(id)sender {
     [self loadPageinWebView:@"http://www.videocareerfinder.com/app/teleprompter"];
}

- (IBAction)mychecklist_btn:(id)sender {
     [self loadPageinWebView:@"http://www.videocareerfinder.com/app/my_check_list"];
}

- (IBAction)whatnow_btn:(id)sender {
     [self loadPageinWebView:@"http://www.videocareerfinder.com/app/what_now"];
}


-(void)showTelePrompter {
    telescroller.hidden = false;
    mywebview.hidden = true;
}

-(NSString *)getCurrentScriptType {
    return myglob;
}

- (IBAction)candidate_scripts:(id)sender {
    myglob = @"Candidate";
    //Load first Candidate script
    NSString *currscripttext = [candidatescript_array objectAtIndex:0];
    [self showTelePrompter];
    [mytele setString:currscripttext];
    [self showPanel2_lower];
}


- (IBAction)student_scripts:(id)sender {
   myglob = @"Student";
    //Load first Student script
    NSString *currscripttext = [studentscript_array objectAtIndex:0];
    [self showTelePrompter];
    
    [mytele setString:currscripttext];
  [self showPanel2_lower];
    
}

- (IBAction)recentgrad_scripts:(id)sender {
   myglob = @"Graduate";
    //Load first Grad script
    NSString *currscripttext = [recentgradscript_array objectAtIndex:0];
    [self showTelePrompter];
    
    [mytele setString:currscripttext];
  [self showPanel2_lower];
    
}

- (IBAction)military_scripts:(id)sender {
   myglob = @"Military";
    //Load first Military script
    NSString *currscripttext = [militaryscript_array objectAtIndex:0];
    [self showTelePrompter];
    
    [mytele setString:currscripttext];
  [self showPanel2_lower];
}

-(void) LoadThisScript:(NSInteger)button_num {
    NSString *sctype = [self getCurrentScriptType];
    NSInteger scriptnum = button_num - 1;
    if ([sctype isEqualToString:@"Military"]){
       
        NSString *currscripttext = [militaryscript_array objectAtIndex:scriptnum];
        [self showTelePrompter];
        
        [mytele setString:currscripttext];
    }
    
    if ([sctype isEqualToString:@"Candidate"]){
        
        NSString *currscripttext = [candidatescript_array objectAtIndex:scriptnum];
        [self showTelePrompter];
        
        [mytele setString:currscripttext];
    }
    
    if ([sctype isEqualToString:@"Student"]){
        
        NSString *currscripttext = [studentscript_array objectAtIndex:scriptnum];
        [self showTelePrompter];
        
        [mytele setString:currscripttext];
    }
    
    if ([sctype isEqualToString:@"Graduate"]){
        
        NSString *currscripttext = [recentgradscript_array objectAtIndex:scriptnum];
        [self showTelePrompter];
        
        [mytele setString:currscripttext];
    }
    


    
    
}



-(void) doAnimation:(NSString *) speedValue {
    
    NSTimeInterval myspeed = 20.0f;
    NSString *currSpeed = speedValue ;
    if ([currSpeed  isEqual: @"2"]) {
        myspeed = 30.0f;
    }
    
    if ([currSpeed  isEqual: @"1"]) {
        myspeed = 40.0f;
    }
    if ([currSpeed  isEqual: @"4"]) {
        myspeed = 10.0f;
    }
    if ([currSpeed  isEqual: @"5"]) {
        myspeed = 5.0f;
    }
    if ([currSpeed  isEqual: @"3"]) {
        myspeed = 20.0f;
    }
    
    
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:myspeed];
    [[NSAnimationContext currentContext] setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
	NSClipView* clipView = [[mytele enclosingScrollView] contentView];
	NSPoint startpoint = [clipView bounds].origin;
    startpoint.y = 0;
    [clipView setBoundsOrigin:startpoint];
    NSPoint newOrigin = [clipView bounds].origin;
    newOrigin.y = [mytele frame].size.height;
	[[clipView animator] setBoundsOrigin:newOrigin];
	[NSAnimationContext endGrouping];
    
    
    
    
}



- (IBAction)script_1:(id)sender {
    [self LoadThisScript:1];
    
}

- (IBAction)script_2:(id)sender {
    [self LoadThisScript:2];

}

- (IBAction)script_3:(id)sender {
    [self LoadThisScript:3];

}

- (IBAction)script_4:(id)sender {
    [self LoadThisScript:4];

}

- (IBAction)script_5:(id)sender {
    [self LoadThisScript:5];

}

- (IBAction)script_6:(id)sender {
    [self LoadThisScript:6];

}

- (IBAction)script_7:(id)sender {
    [self LoadThisScript:7];

}

- (IBAction)script_8:(id)sender {
    [self LoadThisScript:8];

}

- (IBAction)script_9:(id)sender {
    [self LoadThisScript:9];

}

- (IBAction)script_10:(id)sender {
    [self LoadThisScript:10];

}

- (IBAction)update_Scripts_btn:(id)sender {
    //get latest xml data from site
   NSURL *myurl = [NSURL URLWithString:@"http://www.videocareerfinder.com/scripts/vcf-scripts.xml"];
 
    NSData* data = [NSData dataWithContentsOfURL:myurl];
     NSBundle *mainbundle = [NSBundle mainBundle];
    NSString *myFile = [mainbundle pathForResource: @"vcf-scripts" ofType: @"xml"];
   //  Save latest xml to local drive
     [data writeToFile:myFile atomically:YES];
   
    //Re-populate arrays from updated file.
     [self loadScriptsfromXML];
        
}

- (IBAction)left_align_btn:(id)sender {
    [mytele alignLeft:sender];
}

- (IBAction)center_align_btn:(id)sender {
    [mytele alignCenter:sender];
}

- (IBAction)black_on_white_btn:(id)sender {
    mytele.backgroundColor = [NSColor whiteColor];
    mytele.textColor = [NSColor blackColor];
    [mytele setInsertionPointColor:[NSColor blackColor]];
   
 //   [mytele setFont:[NSFont fontWithName:@"Courier" size:20]];
}

- (IBAction)white_on_black_btn:(id)sender {
    
    mytele.backgroundColor = [NSColor blackColor];
    mytele.textColor = [NSColor whiteColor];
    [mytele setInsertionPointColor:[NSColor whiteColor]];
}

- (IBAction)fontsize_slider:(id)sender {
    NSString *currFontSize = [fontsize_slider_val stringValue];
    
    if([currFontSize isEqualToString:@"3"]) {
         [mytele setFont:[NSFont fontWithName:@"Courier" size:13]];
    }
   
    if([currFontSize isEqualToString:@"4"]) {
        [mytele setFont:[NSFont fontWithName:@"Courier" size:16]];
    }
    if([currFontSize isEqualToString:@"5"]) {
        [mytele setFont:[NSFont fontWithName:@"Courier" size:19]];
    }
    if([currFontSize isEqualToString:@"2"]) {
        [mytele setFont:[NSFont fontWithName:@"Courier" size:11]];
    }
    if([currFontSize isEqualToString:@"1"]) {
        [mytele setFont:[NSFont fontWithName:@"Courier" size:9]];
    }
}
- (IBAction)test_Scroll_btn:(id)sender {
    
    NSString *currspeed = [speed_slider_val stringValue];
    [self doAnimation:currspeed];
}

- (IBAction)speed_slider:(id)sender {
    NSString *currspeed = [speed_slider_val stringValue];
    [self doAnimation:currspeed];
}


-(void) stoptelescroll {
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:0.0f];
    [[NSAnimationContext currentContext] setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
	NSClipView* clipView = [[mytele enclosingScrollView] contentView];
    NSPoint newOrigin = [clipView bounds].origin;
    newOrigin.y = 0;
	[[clipView animator] setBoundsOrigin:newOrigin];
	[NSAnimationContext endGrouping];
   
    
}


- (IBAction)stop_test_scroll:(id)sender {
    [self stoptelescroll];
}

-(void) hideVideoWindows {
    mMovieView.hidden = true;
    mCaptureView.hidden = true;
    record_btn.hidden = true;
    play_btn.hidden = true;
    stop_btn.hidden = true;
    upload_btn.hidden = true;
    
    
}

-(void) startCam {

    mMovieView.hidden = false;
    mCaptureView.hidden = false;
    record_btn.hidden = false;
    play_btn.hidden = false;
    stop_btn.hidden = false;
    upload_btn.hidden = false;
    
    mCaptureSession = [[QTCaptureSession alloc] init];
	BOOL success = NO;
    NSError *error;
	
	QTCaptureDevice *videoDevice = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeVideo];
	success = [videoDevice open:&error];
	
	QTCaptureDevice *audioDevice = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeSound];
	success = [audioDevice open:&error];
	
	
	
    if (videoDevice && audioDevice) {
        success = [videoDevice open:&error];
        if (!success) {
        }
		
		success = [audioDevice open:&error];
        if (!success) {
        }
        
		video = [[QTCaptureDeviceInput alloc] initWithDevice:videoDevice];
		success = [mCaptureSession addInput:video error:&error];
		
		audio = [[QTCaptureDeviceInput alloc] initWithDevice:audioDevice];
		success = [mCaptureSession addInput:audio error:&error];
		
		
        if (!success) {
            // Handle error
        }
		
		mCaptureMovieFileOutput = [[QTCaptureMovieFileOutput alloc] init];
		success = [mCaptureSession addOutput:mCaptureMovieFileOutput error:&error];
		if (!success) {
		}
		[mCaptureMovieFileOutput setDelegate:self];
		
		NSEnumerator *connectionEnumerator = [[mCaptureMovieFileOutput connections] objectEnumerator];
        QTCaptureConnection *connection;
		
		while ((connection = [connectionEnumerator nextObject])) {
            NSString *mediaType = [connection mediaType];
            QTCompressionOptions *compressionOptions = nil;
            if ([mediaType isEqualToString:QTMediaTypeVideo]) {
                compressionOptions = [QTCompressionOptions compressionOptionsWithIdentifier:@"QTCompressionOptionsJPEGVideo"];
            } else if ([mediaType isEqualToString:QTMediaTypeSound]) {
                compressionOptions = [QTCompressionOptions compressionOptionsWithIdentifier:@"QTCompressionOptionsHighQualityAACAudio"];
            }
			
            [mCaptureMovieFileOutput setCompressionOptions:compressionOptions forConnection:connection];
			[mCaptureView setCaptureSession:mCaptureSession];
		}
		
		
		[mCaptureSession startRunning];
		//startCountdown();
	}

    
}



- (IBAction)initiate_Recording:(id)sender {
    
    NSFileManager *manager = [NSFileManager defaultManager];
	
		if(![manager fileExistsAtPath:[NSString stringWithFormat:@"%@/vcf.mov", [[NSFileManager defaultManager] applicationSupportDirectory]]])
	{
		[play_btn setEnabled:FALSE];
		
		
	}
	[stop_btn setEnabled:FALSE];

//	[self makeKeyAndOrderFront:nil];
//	[NSApp activateIgnoringOtherApps:YES];
	
	    
    
    [self startCam];
}


- (IBAction)record_btn_press:(id)sender {
    
    isrecord = 1;
	//pauseTimer = 0;
	
    NSLog(@"STARTING RECORDING");
	[mCaptureMovieFileOutput recordToOutputFileURL:[NSURL fileURLWithPath:(@"%@",[NSString stringWithFormat:@"%@/vcf.mov", [[NSFileManager defaultManager] applicationSupportDirectory]])]];
	counter = 0;
	
	
	[play_btn setEnabled:FALSE];
	[record_btn setEnabled:FALSE];
//	[keepBtn setEnabled:FALSE];
	[stop_btn setEnabled:TRUE];
    
    //Show teleprompter
    if ([panel3_manual_Scroll_btn state] == NSOffState)
    {
        NSString *currspeed = [speed_slider_val stringValue];
        [self doAnimation:currspeed];
    }
    
    
    
}




- (void)movieLoadStateDidChange:(NSNotification *)notification
{
	// First make sure that this notification is for our movie.
	
    if([newMovie rate] == 0)
    {
        newMovie = nil;
        [mMovieView setMovie:newMovie];
                
        NSLog(@"STARTING CAMERA");
        [mCaptureView setHidden:NO];
        [play_btn setEnabled:TRUE];
        [record_btn setEnabled:TRUE];
      //  [keepBtn setEnabled:TRUE];
        [self startCam];
      
    }
}	



- (IBAction)play_btn_press:(id)sender {
    NSLog(@"PLAYING MOVIE");
    isrecord = 0;
	[mCaptureSession stopRunning];
    [[video device] close];
	[[audio device] close];
	[mCaptureView setHidden:YES];
	
	[play_btn setEnabled:FALSE];
	[record_btn setEnabled:FALSE];
	
/*
	//sIMULATE movie playing
    
    NSLog(@"STARTING CAMERA");
    [mCaptureView setHidden:NO];
    [play_btn setEnabled:TRUE];
    [record_btn setEnabled:TRUE];
    //  [keepBtn setEnabled:TRUE];
    [self startCam];
    
 */
    
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(movieLoadStateDidChange:)
												 name:QTMovieRateDidChangeNotification
											   object:nil];
  
    
    
	newMovie = [QTMovie movieWithURL:[NSURL fileURLWithPath:(@"%@",[NSString stringWithFormat:@"%@/vcf.mov", [[NSFileManager defaultManager] applicationSupportDirectory]])] error:nil];
	[mMovieView setMovie:newMovie];
	[newMovie play];
    
    
}


- (IBAction)stop_btn_press:(id)sender {
    [self stoptelescroll];
    
   
    
    
          if(isrecord==1)
	{
        NSLog(@"RECORD=1 ENDING RECORDING");
		//pauseTimer = 1;
		counter = 0;
		[mCaptureMovieFileOutput recordToOutputFileURL:nil];
		[play_btn setEnabled:TRUE];
		[record_btn setEnabled:TRUE];
		//[keepBtn setEnabled:TRUE];
		[stop_btn setEnabled:FALSE];
        	}
	else if(isrecord==0)
	{
         NSLog(@"RECORD=0");
     //   [mCaptureMovieFileOutput recordToOutputFileURL:nil];
      	[mMovieView setMovie:nil];
		[mCaptureView setHidden:NO];
		[self startCam];
		[play_btn setEnabled:TRUE];
		[record_btn setEnabled:TRUE];
		//[keepBtn setEnabled:TRUE];
		[stop_btn setEnabled:FALSE];
       
	}
    
    
}

- (IBAction)upload_btn_press:(id)sender {
    
    [mCaptureSession stopRunning];
    [[video device] close];
	[[audio device] close];
        
    Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step4_new"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];
    
}

- (IBAction)next1:(id)sender {
    [self showPanel2];
}

- (IBAction)close_app:(id)sender {
    [NSApp terminate: nil];
    
}






- (IBAction)next2:(id)sender {
    [self showPanel3];
}
- (IBAction)next3:(id)sender {
    [self showPanel4];
}


- (IBAction)next4:(id)sender {
    [mCaptureSession stopRunning];
    [[video device] close];
	[[audio device] close];
    
    Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step4_new"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];

}
- (IBAction)back_btn:(id)sender {
    [mCaptureSession stopRunning];
    [[video device] close];
	[[audio device] close];
    
    Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step1"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];

}
@end
