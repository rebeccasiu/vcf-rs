//
//  AlternateImageBtn.m
//  Videocareerfinder
//
//  Created by Anderson Homer on 7/24/13.
//
//

#import "AlternateImageBtn.h"

@implementation AlternateImageBtn


- (void)updateTrackingAreas
{
	[super updateTrackingAreas];
	
	if (trackingArea)
	{
		[self removeTrackingArea:trackingArea];
		[trackingArea release];
	}
	
	NSTrackingAreaOptions options = NSTrackingInVisibleRect | NSTrackingMouseEnteredAndExited | NSTrackingActiveInKeyWindow;
	trackingArea = [[NSTrackingArea alloc] initWithRect:NSZeroRect options:options owner:self userInfo:nil];
	[self addTrackingArea:trackingArea];
    
    truealtimage = [super alternateImage];
    trueimage = [super image];
   
}



- (void)mouseEntered:(NSEvent *)event
{
    [self setImage:truealtimage];
    
}

- (void)mouseExited:(NSEvent *)event
{
	
    [self setImage:trueimage];
}



@end
