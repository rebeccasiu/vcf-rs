//
//  Step4_new.h
//  Videocareerfinder
//
//  Created by Anderson Homer on 7/25/13.
//
//

#import <Cocoa/Cocoa.h>


@interface Step4_new : NSWindow
{
    NSPoint initialLocation;
    NSTextField *confirm_email_address;
    NSTextField *register_message;
    NSTextField *first_name;
    NSTextField *last_name;
    NSTextField *email_address;
    NSSecureTextField *quick_password;
    NSSecureTextField *quick_password_confirm;
    NSTextField *username;
    NSSecureTextField *upassword;
    NSImageView *username_light;
    NSImageView *password_light;
    NSTextField *login_message;
}

@property (assign) NSPoint initialLocation;

@property (assign) IBOutlet NSTextField *username;

@property (assign) IBOutlet NSSecureTextField *upassword;
@property (assign) IBOutlet NSImageView *username_light;
@property (assign) IBOutlet NSImageView *password_light;
- (IBAction)login_upload_btn:(id)sender;
- (IBAction)quick_reg:(id)sender;
@property (assign) IBOutlet NSTextField *login_message;
- (IBAction)close_app:(id)sender;

@property (assign) IBOutlet NSTextField *register_message;
@property (assign) IBOutlet NSTextField *first_name;
@property (assign) IBOutlet NSTextField *email_address;
@property (assign) IBOutlet NSTextField *last_name;
@property (assign) IBOutlet NSTextField *confirm_email_address;
@property (assign) IBOutlet NSSecureTextField *quick_password;
@property (assign) IBOutlet NSSecureTextField *quick_password_confirm;
- (IBAction)back_btn:(id)sender;

@end
