//
//  Untitled_AppDelegate.m
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import "GlobalVars.h"
#import "Step9.h"
#import <AppKit/AppKit.h>
#import "Controller.h"
#import "NSFileManager+DirectoryLocations.h"
#import "ASIFormDataRequest.h"
#import "MyUploader.h"
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import <AVFoundation/AVBase.h>

@implementation Step9

@synthesize initialLocation;
@synthesize loadimage;

/*
- (void)advanceTimer:(NSTimer *)timer
{
    
   
	 
    counter = counter + 1;
    if (counter > 1) {
		[timer invalidate];
		        
              
        
        NSLog(@"Starting encode");
        NSLog(@"pass1");
        
        myFFmpegTask = [[NSTask alloc] init];
        NSBundle *thisBundle = [NSBundle bundleForClass:[self class]];
        NSString *resourcefolder = [thisBundle resourcePath];
        NSString * ffmpeglaunch = [[NSBundle mainBundle] pathForResource:@"ffmpeg" ofType:nil];
        NSString * presetpath = [[NSBundle mainBundle] pathForResource:@"libx264-normal.ffpreset" ofType:nil];
        NSString * passlogfile = [[NSBundle mainBundle] pathForResource:@"ffmpeglog" ofType:nil];
        NSLog(@"%@",passlogfile);
        NSPipe * pipe = [[NSPipe alloc] init];
        //[myFFmpegTask setStandardError: pipe];
        [myFFmpegTask setStandardOutput: pipe];
        [myFFmpegTask setLaunchPath:ffmpeglaunch];
        [myFFmpegTask setArguments:[NSArray arrayWithObjects:@"-y",@"-i",[NSString stringWithFormat:@"%@/output.mov", [[NSFileManager defaultManager] applicationSupportDirectory]],@"-b",@"900k",@"-r",@"25",@"-vcodec",@"libx264",@"-bt",@"4M",@"-s",@"640x480",@"-aspect",@"4:3",@"-fpre",presetpath,@"-strict",@"experimental",@"-ac",@"2",@"-ar",@"44100",@"-ab",@"160k",[NSString stringWithFormat:@"%@/tagged.mp4", [[NSFileManager defaultManager] applicationSupportDirectory]],nil]];
        
        [myFFmpegTask launch];
        
        
        
        NSFileHandle *file;
        file = [pipe fileHandleForReading];

        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self
                   selector:@selector(myThread:)
                       name:NSTaskDidTerminateNotification
                     object:myFFmpegTask];
 			
		
        
	}
	
	
}
*/

-(void) StartEncode {
   
    NSLog(@"Starting encode");
    NSLog(@"pass1");
    
    myFFmpegTask = [[NSTask alloc] init];
    NSBundle *thisBundle = [NSBundle bundleForClass:[self class]];
    NSString *resourcefolder = [thisBundle resourcePath];
    NSString * ffmpeglaunch = [[NSBundle mainBundle] pathForResource:@"ffmpeg" ofType:nil];
    NSString * presetpath = [[NSBundle mainBundle] pathForResource:@"libx264-normal.ffpreset" ofType:nil];
    NSString * passlogfile = [[NSBundle mainBundle] pathForResource:@"ffmpeglog" ofType:nil];
    NSLog(@"%@",passlogfile);
    NSPipe * pipe = [[NSPipe alloc] init];
    //[myFFmpegTask setStandardError: pipe];
    [myFFmpegTask setStandardOutput: pipe];
    [myFFmpegTask setLaunchPath:ffmpeglaunch];
    [myFFmpegTask setArguments:[NSArray arrayWithObjects:@"-y",@"-i",[NSString stringWithFormat:@"%@/output.mov", [[NSFileManager defaultManager] applicationSupportDirectory]],@"-b",@"900k",@"-r",@"25",@"-vcodec",@"libx264",@"-bt",@"4M",@"-s",@"640x480",@"-aspect",@"4:3",@"-fpre",presetpath,@"-strict",@"experimental",@"-ac",@"2",@"-ar",@"44100",@"-ab",@"160k",[NSString stringWithFormat:@"%@/tagged.mp4", [[NSFileManager defaultManager] applicationSupportDirectory]],nil]];
    
    [myFFmpegTask launch];
    
    
    
    NSFileHandle *file;
    file = [pipe fileHandleForReading];
    
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(myThread:)
                   name:NSTaskDidTerminateNotification
                 object:myFFmpegTask];
    
    
   
   [myFFmpegTask waitUntilExit];
    
  //  NSLog(@"Boom");
    
    
}



- (void)loadTimer:(NSTimer *)timer
{
   	imgcounter = imgcounter + 1;
	if(imgcounter==1)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_1.png"]];
	}
	else if(imgcounter==2)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_2.png"]];
	}
	else if(imgcounter==3)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_3.png"]];
	}
	else if(imgcounter==4)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_4.png"]];
	}
	else if(imgcounter==5)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_5.png"]];
	}
	else if(imgcounter==6)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_6.png"]];
	}
	else if(imgcounter==7)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_7.png"]];
	}
	else if(imgcounter==8)
	{
		[loadimage setImage:[NSImage imageNamed:@"step5_load_8.png"]];
		imgcounter = 0;
	}
	
	
	
}


- (void) mergeTwoVideo
{
    NSLog(@"Starting merge");
    
    AVMutableComposition* composition = [AVMutableComposition composition];
    
    NSString* path1 = [[NSBundle mainBundle] pathForResource:@"tagm.mov" ofType:nil];
    NSString* path2 = [NSString stringWithFormat:@"%@/vcf.mov", [[NSFileManager defaultManager] applicationSupportDirectory]];
    
    AVURLAsset* video1 = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:path1] options:nil];
    
    AVURLAsset* video2 = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:path2] options:nil];
    
    AVMutableCompositionTrack * composedTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                 
                                                                         preferredTrackID:kCMPersistentTrackID_Invalid];
    
    
    AVMutableCompositionTrack *composedAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio  preferredTrackID:kCMPersistentTrackID_Invalid];
    
    
    [composedTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, video1.duration)
     
                           ofTrack:[[video1 tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
     
                            atTime:kCMTimeZero
     
                             error:nil];
    
    
    [composedAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, video1.duration)
     
                                ofTrack:[[video1 tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
     
                                 atTime:kCMTimeZero
     
                                  error:nil];
    
    
    
    
    [composedTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, video2.duration)
     
                           ofTrack:[[video2 tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
     
                            atTime:video1.duration
     
                             error:nil];
    
    [composedAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, video2.duration)
     
                                ofTrack:[[video2 tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
     
                                 atTime:video1.duration
     
                                  error:nil];
    
    
    
    
    
    //   NSString* documentsDirectory= [selfapplicationDocumentsDirectory];
    
    NSString* myDocumentPath= [NSString stringWithFormat:@"%@/output.mov", [[NSFileManager defaultManager] applicationSupportDirectory]];
    
    NSURL *url = [[NSURL alloc] initFileURLWithPath: myDocumentPath];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:myDocumentPath])
        
    {
        
        [[NSFileManager defaultManager] removeItemAtPath:myDocumentPath error:nil];
        
    }
    
    AVAssetExportSession *exporter = [[[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPreset640x480] autorelease];
    
    
    
    exporter.outputURL=url;
    
    exporter.outputFileType = @"com.apple.quicktime-movie";
    
    exporter.shouldOptimizeForNetworkUse = YES;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        
        
        NSLog(@"Completed merge");
        [self StartEncode];
        
        
        switch ([exporter status]) {
                
            caseAVAssetExportSessionStatusFailed:
            {
                NSLog(@"Failed ?");
                
                break;
            }
                
            caseAVAssetExportSessionStatusCancelled:
            {   NSLog(@"Cancelled ?");
                break;
                
            }
                
            caseAVAssetExportSessionStatusCompleted:
            {
                NSLog(@"Successful ?");
                
                
                break;
            }
                
            default:
                
                break;
                
        }
        
    }];
    
}


- (void)upload
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *username = nil;

    if (standardUserDefaults)
        username = [standardUserDefaults objectForKey:@"username"];



    [[MyUploader alloc]initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://www.videocareerfinder.com/app/upload2/%@", username]]
                          filePath: [NSString stringWithFormat:@"%@/tagged.mp4", [[NSFileManager defaultManager] applicationSupportDirectory]]
                          delegate: self
                      doneSelector: @selector(onUploadDone)
                     errorSelector: @selector(onUploadError)];
}


-(void)onUploadDone
{
	Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step10"];

	[myController showWindow:nil];
	[[myController window] makeKeyAndOrderFront:nil];
	[self close];
	[self release];
}

-(void)onUploadError
{
	NSAlert *alert = [[[NSAlert alloc] init] autorelease];
	[alert setMessageText:@"Error"];
	[alert setInformativeText:@"An error has occured during process. Please go back and restart the process."];
	[alert setAlertStyle:NSInformationalAlertStyle];
	[alert beginSheetModalForWindow:self modalDelegate:self didEndSelector:nil contextInfo:nil];
}

- (void)myThread:(NSNotification *)note
{
	NSTask *_currentFFMPEGTask = [note object];
	NSLog(@"myThread Started");
	int status = [_currentFFMPEGTask terminationStatus];
	if (status == 0) {
		
		       
       [_currentFFMPEGTask terminate];
        [myFFmpegTask terminate];
        [myimagetimer invalidate];
        
       
    //    [self mergeTwoVideo];
        //Load next screen.
        
   /*
        Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step6_new"];
        
        [myController showWindow:nil];
        [[myController window] makeKeyAndOrderFront:nil];
        [self close];
        [self release];
*/
        [self upload];
    }
	
}



- (void)awakeFromNib
{
    
    [self mergeTwoVideo];
   
    
  /*  NSTimer *countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1
															   target:self
															 selector:@selector(advanceTimer:)
															 userInfo:nil
															  repeats:YES];*/
    

    myimagetimer = [[NSTimer scheduledTimerWithTimeInterval:0.3
                                                     target:self
                                                   selector:@selector(loadTimer:)
                                                   userInfo:nil
                                                    repeats:YES] retain];

    //merge process starts here
    
    
	
}




- (void)dealloc
{
	
	[super dealloc] ;
}



/***********************************/

- (id)initWithContentRect:(NSRect)contentRect
				styleMask:(NSUInteger)windowStyle
				  backing:(NSBackingStoreType)bufferingType
					defer:(BOOL)deferCreation
{
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    
	[self setLevel: NSPopUpMenuWindowLevel];
	
	[self center];
	
    return self;	
}

- (void)mouseDown:(NSEvent *)theEvent {    
    // Get the mouse location in window coordinates.
    self.initialLocation = [theEvent locationInWindow];
}


- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect windowFrame = [self frame];
    NSPoint newOrigin = windowFrame.origin;
	
    // Get the mouse location in window coordinates.
    NSPoint currentLocation = [theEvent locationInWindow];
    // Update the origin with the difference between the new mouse location and the old mouse location.
    newOrigin.x += (currentLocation.x - initialLocation.x);
    newOrigin.y += (currentLocation.y - initialLocation.y);
	
    // Don't let window get dragged up under the menu bar
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height)) {
        newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    // Move the window to the new location
    [self setFrameOrigin:newOrigin];
}


@end
