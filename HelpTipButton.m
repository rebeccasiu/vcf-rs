//
//  HelpTipButton.m
//  Videocareerfinder
//
//  Created by Anderson Homer on 7/24/13.
//
//

#import "HelpTipButton.h"

@implementation HelpTipButton

- (void) dealloc{
    [super dealloc];
}

- (void)updateTrackingAreas
{
	[super updateTrackingAreas];
	
	if (trackingArea)
	{
		[self removeTrackingArea:trackingArea];
		[trackingArea release];
	}
	
	NSTrackingAreaOptions options = NSTrackingInVisibleRect | NSTrackingMouseEnteredAndExited | NSTrackingActiveInKeyWindow;
	trackingArea = [[NSTrackingArea alloc] initWithRect:NSZeroRect options:options owner:self userInfo:nil];
	[self addTrackingArea:trackingArea];
    
    truealtimage = [super alternateImage];
    trueimage = [super image];
    
}



- (void)mouseEntered:(NSEvent *)event
{
    [self setImage:truealtimage];
    NSString *imagetosend = [trueimage name];
    NSString *prefix = @"text_";
    imagetosend = [prefix stringByAppendingString:imagetosend];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"myTestNotification"
     object:imagetosend];
}

- (void)mouseExited:(NSEvent *)event
{
	
    [self setImage:trueimage];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"myTestNotification"
     object:@"hide"];
    
}



@end
