//
//  MyUploader.m
//  Videocareerfinder
//
//  Created by Anderson Homer on 8/5/13.
//
//

#import "ASIFormDataRequest.h"

#import "MyUploader.h"


#define ASSERT(x) NSAssert(x, @"")
#define Log(x,y)

@implementation MyUploader

- (id)initWithURL: (NSURL *)aServerURL   // IN
         filePath: (NSString *)aFilePath // IN
         delegate: (id)aDelegate         // IN
     doneSelector: (SEL)aDoneSelector    // IN
    errorSelector: (SEL)anErrorSelector  // IN
{
	if ((self = [super init])) {
		ASSERT(aServerURL);
		ASSERT(aFilePath);
		ASSERT(aDelegate);
		ASSERT(aDoneSelector);
		ASSERT(anErrorSelector);
		
		serverURL = [aServerURL retain];
		filePath = [aFilePath retain];
		delegate = [aDelegate retain];
		doneSelector = aDoneSelector;
		errorSelector = anErrorSelector;
		
		[self upload];
	}
	return self;
}



-(void)upload {
    
    NSLog(@"Uploading.....");
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:serverURL];

    
    NSData *imageData = [NSData dataWithContentsOfFile:filePath];
    
    [request setData:imageData withFileName:@"tagged.mp4" andContentType:@"video/mp4" forKey:@"file"];
    
    [request setRequestMethod:@"POST"];
    
    [request setDelegate:self];
    
    [request startAsynchronous];

    
    
    
    
}


- (void)requestFinished:(ASIHTTPRequest *)request
{
    // Use when fetching text data
    NSString *responseString = [request responseString];
    
    // Use when fetching binary data
    NSData *responseData = [request responseData];
    
    NSLog(@"Finished Uploading - Uploader Class");
    NSLog(@"Response string - %@",responseString);
    BOOL success = true;
    
    [delegate performSelector:success ? doneSelector : errorSelector
				   withObject:self];
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"Failed");
    
    BOOL success = false;
    
    [delegate performSelector:success ? doneSelector : errorSelector
				   withObject:self];
}



@end


