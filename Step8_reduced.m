//
//  Untitled_AppDelegate.m
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import "GlobalVars.h"
#import "Step8_reduced.h"
#import <AppKit/AppKit.h>
#import <QTKit/QTKit.h>

#import "Controller.h"
#import "NSFileManager+DirectoryLocations.h"

@implementation Step8_reduced

@synthesize initialLocation,playBtn, stopBtn, recordBtn, keepBtn, counter;


- (void)advanceTimer:(NSTimer *)timer
{
    counter = counter + 1;
    [countdownField setStringValue:([NSString stringWithFormat:@"00:%d", counter])];
    if (pauseTimer == 1) {
		[timer invalidate];
	}
	else {
		
		if (counter > 74) {
			[timer invalidate]; 
			[mCaptureMovieFileOutput recordToOutputFileURL:nil];
			[playBtn setEnabled:TRUE];
			[recordBtn setEnabled:TRUE];
			[keepBtn setEnabled:TRUE];
		}
	}
	
}

- (void)awakeFromNib
{
	
	NSFileManager *manager = [NSFileManager defaultManager];
	
	pauseTimer = 0;
	
	if(![manager fileExistsAtPath:[NSString stringWithFormat:@"%@/vcf.mov", [[NSFileManager defaultManager] applicationSupportDirectory]]])
	{
		[playBtn setEnabled:FALSE];
		[keepBtn setEnabled:FALSE];
	}
	
	[stopBtn setEnabled:FALSE];
	
	[self makeKeyAndOrderFront:nil];
	[NSApp activateIgnoringOtherApps:YES];
	
    mCaptureSession = [[QTCaptureSession alloc] init];
	BOOL success = NO;
    NSError *error;
	
	QTCaptureDevice *device = [QTCaptureDevice defaultInputDeviceWithMediaType:QTMediaTypeVideo];
    if (device) {
        success = [device open:&error];
        if (!success) {
        }
        mCaptureDeviceInput = [[QTCaptureDeviceInput alloc] initWithDevice:device];
        success = [mCaptureSession addInput:mCaptureDeviceInput error:&error];
        if (!success) {
            // Handle error
        }
		
		mCaptureMovieFileOutput = [[QTCaptureMovieFileOutput alloc] init];
		success = [mCaptureSession addOutput:mCaptureMovieFileOutput error:&error];
		if (!success) {
		}
		[mCaptureMovieFileOutput setDelegate:self];
		
		NSEnumerator *connectionEnumerator = [[mCaptureMovieFileOutput connections] objectEnumerator];
        QTCaptureConnection *connection;
		
        while ((connection = [connectionEnumerator nextObject])) {
            NSString *mediaType = [connection mediaType];
            QTCompressionOptions *compressionOptions = nil;
            if ([mediaType isEqualToString:QTMediaTypeVideo]) {
                compressionOptions = [QTCompressionOptions compressionOptionsWithIdentifier:@"QTCompressionOptions240SizeH264Video"];
            } else if ([mediaType isEqualToString:QTMediaTypeSound]) {
                compressionOptions = [QTCompressionOptions compressionOptionsWithIdentifier:@"QTCompressionOptionsHighQualityAACAudio"];
            }
			
            [mCaptureMovieFileOutput setCompressionOptions:compressionOptions forConnection:connection];
			[mCaptureView setCaptureSession:mCaptureSession];
		}
		
		[mCaptureSession startRunning];
	}
}	

- (IBAction)gotoStep4:(id)sender
{
	
	[mCaptureSession stopRunning];
    [[mCaptureDeviceInput device] close];
	Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step3"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];
	
}
- (IBAction)startRecord:(id)sender
{    
	//NSFileManager *manager = [NSFileManager defaultManager];
	
	[mCaptureMovieFileOutput recordToOutputFileURL:[NSURL fileURLWithPath:(@"%@",[NSString stringWithFormat:@"%@/vcf.mov", [[NSFileManager defaultManager] applicationSupportDirectory]])]];
	counter = 0;
	
	
	[playBtn setEnabled:FALSE];
	[recordBtn setEnabled:FALSE];
	[keepBtn setEnabled:FALSE];
	[stopBtn setEnabled:TRUE];
	
    NSTimer *countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1
															   target:self
															 selector:@selector(advanceTimer:)
															 userInfo:nil
															  repeats:YES];
	
}


- (IBAction)expandApp:(id)sender
{
	[mCaptureSession stopRunning];
    [[mCaptureDeviceInput device] close];
	Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step8"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];
	
	
}

- (IBAction)stopRecord:(id)sender
{
	pauseTimer = 1;  
	[mCaptureMovieFileOutput recordToOutputFileURL:nil];
	[playBtn setEnabled:TRUE];
	[recordBtn setEnabled:TRUE];
	[keepBtn setEnabled:TRUE];
	
}


- (void)windowWillClose:(NSNotification *)notification
{
    [mCaptureSession stopRunning];
    [[mCaptureDeviceInput device] close];
}

-(IBAction)closeApp:(id)sender;
{
	[mCaptureSession stopRunning];
    [[mCaptureDeviceInput device] close];
	//[[audio device] close];
	Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step11"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];
}

- (void)dealloc
{
	[mCaptureSession release];
    [mCaptureDeviceInput release];
    [mCaptureMovieFileOutput release];
	[playBtn release];
	[stopBtn release];
	[recordBtn release];
	[keepBtn release];
	[super dealloc] ;
}



/***********************************/

- (id)initWithContentRect:(NSRect)contentRect
				styleMask:(NSUInteger)windowStyle
				  backing:(NSBackingStoreType)bufferingType
					defer:(BOOL)deferCreation
{
	appStatus = @"8";
	
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    
	
	[self center];
	
	[self setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"reduced_screen.png"]]];
	
	
	
	
    return self;	
}

- (void)mouseDown:(NSEvent *)theEvent {    
    // Get the mouse location in window coordinates.
    self.initialLocation = [theEvent locationInWindow];
}

- (BOOL)canBecomeKeyWindow {
    return YES;
}

- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect windowFrame = [self frame];
    NSPoint newOrigin = windowFrame.origin;
	
    // Get the mouse location in window coordinates.
    NSPoint currentLocation = [theEvent locationInWindow];
    // Update the origin with the difference between the new mouse location and the old mouse location.
    newOrigin.x += (currentLocation.x - initialLocation.x);
    newOrigin.y += (currentLocation.y - initialLocation.y);
	
    // Don't let window get dragged up under the menu bar
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height)) {
        newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    // Move the window to the new location
    [self setFrameOrigin:newOrigin];
}

@end