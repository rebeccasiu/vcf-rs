//
//  HoverButton.m
//  HoverButton
//

#import "StopBtn.h"


@implementation StopBtn

- (void)updateTrackingAreas
{
	[super updateTrackingAreas];
	
	if (trackingArea)
	{
		[self removeTrackingArea:trackingArea];
		[trackingArea release];
	}
	
	NSTrackingAreaOptions options = NSTrackingInVisibleRect | NSTrackingMouseEnteredAndExited | NSTrackingActiveInKeyWindow;
	trackingArea = [[NSTrackingArea alloc] initWithRect:NSZeroRect options:options owner:self userInfo:nil];
	[self addTrackingArea:trackingArea];
}

- (void)mouseEntered:(NSEvent *)event
{
	[self setImage:[NSImage imageNamed:@"stop_btn_hover"]];
}

- (void)mouseExited:(NSEvent *)event
{
	[self setImage:[NSImage imageNamed:@"stop_btn"]];
}

@end