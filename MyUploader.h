//
//  MyUploader.h
//  Videocareerfinder
//
//  Created by Anderson Homer on 8/5/13.
//
//

#import <Foundation/Foundation.h>

@interface MyUploader : NSObject <ASIHTTPRequestDelegate>

{
	NSURL *serverURL;
	NSString *filePath;
	id delegate;
	SEL doneSelector;
	SEL errorSelector;
	
	BOOL uploadDidSucceed;
}

- (id)initWithURL: (NSURL *)serverURL
         filePath: (NSString *)filePath
         delegate: (id)delegate
     doneSelector: (SEL)doneSelector
    errorSelector: (SEL)errorSelector;



@end
