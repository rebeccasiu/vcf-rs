//
//  GlobalVars.h
//  VideoCareerFinder
//
//  Created by Ronak on 04/07/11.
//  Copyright 2011 Soft 'n' Web. All rights reserved.
//



@interface GlobalVars : NSObject  {
	
}

extern NSString *appKey;
extern NSString *appStatus;
extern NSString *appUsername;
extern NSString *appFirstLastName;

+ (void)setFullName:(NSString *)name;
+ (NSString *)getFullName;

+ (void)setFirstName:(NSString *)name;
+ (NSString *)getFirstName;

+ (void)setLastName:(NSString *)name;
+ (NSString *)getLastName;

+ (void)setUsername:(NSString *)name;
+ (NSString *)getUsername;

+ (void)setScript:(NSString *)name;
+ (NSString *)getScript;

+ (void)setAlignment:(NSString *)name;
+ (NSString *)getAlignment;

+ (void)setColor:(NSString *)name;
+ (NSString *)getColor;

+ (void)setFont:(NSString *)name;
+ (NSString *)getFont;

+ (void)setSpeed:(NSString *)name;
+ (NSString *)getSpeed;

@end
