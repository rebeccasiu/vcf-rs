//
//  Untitled_AppDelegate.m
//  Untitled
//
//  Created by Ronak on 30/06/11.
//  Copyright Soft 'n' Web 2011 . All rights reserved.
//

#import "GlobalVars.h"
#import "Step10.h"
#import <AppKit/AppKit.h>
#import "Controller.h"
#import "NSFileManager+DirectoryLocations.h"

QTMovie *newMovie;

@implementation Step10

@synthesize initialLocation;

@synthesize playprev_btn;
@synthesize close_btn;
@synthesize continue_btn;
@synthesize record_btn;
@synthesize name_label;

- (void)dealloc
{
	
	[super dealloc] ;
}

- (void)awakeFromNib
{
   
	NSLog(@"Waiting for click");
   
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userfullname = nil;
    
    if (standardUserDefaults)
        userfullname = [standardUserDefaults objectForKey:@"fullname"];
    
   
    NSLog(userfullname);
    
     [name_label setStringValue:userfullname];
    //NSLog(@"%@",[[NSFileManager defaultManager] applicationSupportDirectory]);
}


- (BOOL)canBecomeKeyWindow {
    return YES;
}

-(IBAction)closeApp:(id)sender;
{
	[NSApp terminate: nil];
}

- (IBAction)playpreview:(id)sender {
    
    [self setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"step6_overlay.png"]]];
    [playprev_btn setHidden:YES];
    [continue_btn setHidden:YES];
    [record_btn setHidden:YES];
    [close_btn setHidden:NO];
    [mMovieView setHidden:NO];
    [name_label setHidden:YES];
    
    NSLog(@"Playing movie");
    
    //Attempt to play movie.
    newMovie = [QTMovie movieWithURL:[NSURL fileURLWithPath:(@"%@",[NSString stringWithFormat:@"%@/tagged.mp4", [[NSFileManager defaultManager] applicationSupportDirectory]])] error:nil];
	[mMovieView setMovie:newMovie];
	[newMovie play];
    
}


/***********************************/

- (id)initWithContentRect:(NSRect)contentRect
				styleMask:(NSUInteger)windowStyle
				  backing:(NSBackingStoreType)bufferingType
					defer:(BOOL)deferCreation
{
	self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    
	appStatus = @"10";
	
	[self setLevel: NSPopUpMenuWindowLevel];
	
	[self center];
	
	[self setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"step6_background_image.png"]]];
	
	[self makeKeyAndOrderFront:nil];
	
    return self;	
}

- (void)mouseDown:(NSEvent *)theEvent {    
    // Get the mouse location in window coordinates.
    self.initialLocation = [theEvent locationInWindow];
}



- (void)mouseDragged:(NSEvent *)theEvent {
    NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
    NSRect windowFrame = [self frame];
    NSPoint newOrigin = windowFrame.origin;
	
    // Get the mouse location in window coordinates.
    NSPoint currentLocation = [theEvent locationInWindow];
    // Update the origin with the difference between the new mouse location and the old mouse location.
    newOrigin.x += (currentLocation.x - initialLocation.x);
    newOrigin.y += (currentLocation.y - initialLocation.y);
	
    // Don't let window get dragged up under the menu bar
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height)) {
        newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    // Move the window to the new location
    [self setFrameOrigin:newOrigin];
}

- (IBAction)record_again:(id)sender {
    Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step3_new"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];
}

- (IBAction)continue_uploading:(id)sender {
    Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step7_new"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];
}

- (IBAction)close_preview:(id)sender {
    [self setBackgroundColor:[NSColor colorWithPatternImage:[NSImage imageNamed:@"step6_background_image.png"]]];
	[close_btn setHidden:YES];
    [mMovieView setHidden:YES];
    [playprev_btn setHidden:NO];
    [continue_btn setHidden:NO];
    [record_btn setHidden:NO];
    [name_label setHidden:NO];
    [mMovieView setMovie:nil];
    newMovie = nil;
    
}
- (IBAction)close_app:(id)sender {
    [NSApp terminate: nil];

}

- (IBAction)back_btn:(id)sender {
   
    Controller *myController =  [[Controller alloc] initWithWindowNibName:@"Step3_new"];
	
	[myController showWindow:sender];
	[[myController window] makeKeyAndOrderFront:sender];
	[self close];
	[self release];

    
}
@end
